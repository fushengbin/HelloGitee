    # s1 = b'sdasa'
    # print(type(s1))
# 在创建字符串的时候在字符串前面加一个b就成bytes类型，其所有操作和str一样
# 对于bytes来说：
#   中文：表现形式：s1 = b'啦啦啦'
#         内存中的编码方式：
#   英文：表现形式:s1 = b'dsadsa'
#         内存中的表现形式：可以指定非Unicode
# .encode()将某个对象转化成指定的编码格式
# str --- > bytes 称为编码
#bytes ---> str 称为解码
# .decode（）解码