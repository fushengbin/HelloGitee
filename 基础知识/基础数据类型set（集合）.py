#     set1 = {'alex','alex','TaiBai','WuSir', 1000}
#     print(set1)
# 集合是无序的，并且自带去重。并且里面的元素必须是不可变的数据类型。
# 列表去重的步骤（前提是里面必须是不可变的数据类型），先将列表转换成集合，再将集合转换成列表
#     set1 = {'alex','TaiBai','WuSir'}
#     set1.add('日天')
#     print(set1)
# .add()给集合增加一个元素
#     set1 = {'alex','TaiBai','WuSir'}
#     set1.update('我喜欢你')
#     print(set1)
# .upadte()增加多个元素,但是是将改字符串全部拆开添加到里面
#     set1 = {'alex','TaiBai','WuSir'}
#     set1.remove('alex')
#     print(set1)
# .remove()删除指定元素
#     set1 = {'alex','TaiBai','WuSir'}
#     print(set1.pop())
#     print(set1)
# .pop()随机删除一个,有返回值
#     set1 = {'alex','TaiBai','WuSir'}
#     set1.clear()
#     print(set1)
# .clear()清除整个集合,
#     set1 = {'alex','TaiBai','WuSir'}
#     del set1
#     print(set1)
# 从内存级别删除这个集合,同样适用于其他的数据类型
# 查看集合可以通过for循环，或者直接print
#     set1 = {'1','2','3','4','5'}
#     set2 = {'4','5','6','7','8'}
#     print(set1 & set2)
#     print(set1.intersection(set2))
# .intersection（）和&一样都是查找两个集合的并集，
#     set1 = {'1','2','3','4','5'}
#     set2 = {'4','5','6','7','8'}
#     print(set1 | set2)
#     print(set1.union(set2))
# |和.union()一样都是查找两个集合的交集
#     set1 = {'1','2','3','4','5'}
#     set2 = {'4','5','6','7','8'}
#     print(set1 - set2 )
#     print(set1.difference(set2))
# - 和.diffrernce()查找两个集合之间的差集（差集就是属于集合A但不属于集合B的元素）
#     set1 = {'1','2','3','4','5'}
#     set2 = {'4','5','6','7','8'}
#     print(set1 ^ set2 )
#     print(set1.symmetric_difference(set2))
# ^和.symmetric_difference()一样，就是查找两个集合的对称差集（就是A的差集并上B的差集，就是Cu(A∪B)(A∩B)）
    # set1 = {1,2,3,321321,321314,4143414,54542,434,5,54,'432'}
    # set2 = {1,5,2}
# print(set1 > set2 )  #判断后面那个是不是前面那个的子集（这个还有种说法叫超集也就是说，判断前面那个是不是后面那个的超集）
# print(set1.issuperset(set2))
# print(set2 < set1 ) #判断前面那个是不是后面那个的子集
# print(set2.issubset(set1) )
# < ，> ，.issuperset(),.issubest()判断两个集合那个是子集
#     set1 = {'1','2','3','4','5'}
#     set2 = frozenset(set1)
# forzenset()将集合改变成冻集合（数据不可变0），可以使用for循环，不支持索引