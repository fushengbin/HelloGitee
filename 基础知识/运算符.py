#算术运算：
    # +（加）
    # -（减）
    # *（乘）
    # /（除）
    # %（取余）
    # **(幂次方)
    # //（整除）
        a = 10
        b = 3
        # print(a % b) 取余
        # print(a ** b) 幂次方
        # print( a // b) 整除
    #比较运算：==（相等） ！=（不相等） <>（不相等，不常用） >(大于) <（小于） >=（大于等于） <=（小于等于）
        # print( a == b) 等于
        # print( a != b ) 不等于
        # print( a <> b ) pycharm直接不能识别（应该是在python3.7里面里面取消了这个）
        # print( a > b ) 大于
        # print( a < b ) 小于
        # print( a >= b) 大于等于
        # print( a <= b) 小于等于
    #赋值运算：
        # +=（ a += 1 就是 a = a + 1 同理 a += b 就是 a = a + b ）加等于赋值
            # a += b
        # print( a )
        # -= ( a -= 1 就是 a = a - 1 同理 a -= b 就是 a = a - b ) 减等于赋值
            # a -= b
        # print( a )
        # *=（ a *= 1 就是 a = a * 1 同理 a *= b 就是 a = a * b ）乘等于赋值
            # a *= b
        # print( a )
        # /= ( a /= 1 就是 a = a / 1 同理 a /= b 就是 a = a / b ) 除等于赋值
            # a /= b
        # print( a )
        # %= （ a %= 1 就是 a = a % 1 同理 a %= b 就是 a = a % b ）取余等于赋值
            # a %= b
        # print( a )
        # **= （ a **= 1 就是 a = a ** 1 同理 a **= b 就是 a = a ** b ）幂次方等于赋值
             # a **= b
        # print( a )
        # //= （ a //= 1 就是 a = a // 1 同理 a //= b 就是 a = a // b ）整除等于赋值
            # a //= b
        # print( a )
    #逻辑运算：and  or  not
        # 前提：逻辑运算优先级：() > not > and > or (同一优先级，从左到右依次计算，然后再算下一级)
        #三个情况：1.全是比较运算:
        # print( 2 > 1 and 3 < 4 or 5 > 6 and 1 < 9)
        # 2.全是数字：(and 是显示后面的数，0也是，or显示前面的数，0则显示后面的)
            # print( 1 or 2)
            # print( 0 or 2 )
            # print( 5 or 2)
            # print( 1 or 3)
        #数据类型转换：int  <------> bool （整数到布尔值）
    # int <------> bool ：0转换成bool值为false，非零转换成ture，口诀：非零及true
    # bool<------>int ：ture为1 ，false为0
    # 运算口诀：
        '''x or y if x ture ,return x ,else y.
        x and y if x ture , return y ,else x.'''
    # 3.既有比较运算又有数字
        # print( 1 or 0 and 4 > 2 and 0 ) 一样还是根据运算优先级，然后在根据上面的口诀
    #成员运算： in not in  针对的数据类型：str(字符串)，lisr（列表），tuple（元组），dict（字典），set（集合）等
        # z1 = 'shaohdsadakdask'
        # z2 = 'sh'
        # print( z2 in z2 )
    #就拿上面的来说，如果判断的是so就不行了，因为它是判断那整个字符串是不是在里一个里面