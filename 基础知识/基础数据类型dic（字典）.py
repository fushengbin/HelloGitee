# 用大括号括起来的键值对
# 字典的键不能重复，如果有重复则取后面的哪一个值
dic = {123;123}
a = 'high'
dic[a] = 175
print(dic)
    # 在字典里面添加键值对，格式：字典名 ['键名'] = ['值'].
    # 如果增加的是一个有的键，则修改值，如没有则添加
        # dic = { 'name' : '张胡斌','age' : '你猜','性别' : '你猜' }
        # dic.setdefault('身高','183')
        # print(dic)
    #.setdefault增加一个键值对，用法字典名.setdefaulr（键名，值）
    #如果有改键则不改变任何数据
        # dic = { 'name' : '张胡斌','age' : '你猜','性别' : '你猜' }
        # dic.pop('name')
        # print(dic)
    #.pop（）按照键去删除，中间跟键名，有返回值，返回值是你删除的那个键对应的值
        # dic = { 'name' : '张胡斌','age' : '你猜','性别' : '你猜' }
        # print(dic.pop('name1', '没有这个玩意儿'))
    # 如果说没有这个键，直接会报错，可以在键后面跟一个返回值，即不存在返回什么，但是不会直接打印
        # dic = { 'name' : '张胡斌','age' : '你猜','性别' : '你猜' }
        # dic.clear()
        # print(dic)
    #.clear()清空字典
        # dic = { 'name' : '张胡斌','age' : '你猜','性别' : '你猜' }
        # del dic['name']
        # print(dic)
    #del 删除键值对，没有返回值，你删除的东西如果没有会报错
        # dic = { 'name' : '张胡斌','age' : '你猜','性别' : '你猜','身高':'你猜' }
        # dic2 = { 'name' : '陈海露','age' : '18','性别' : '女','爱好': '张胡斌' }
        # dic.update(dic2)5
        # print(dic)
        # print(dic2)
    #.update()将原字典里面的键值对更新成一个新的字典里面的键值对，
        # dic = { 'name' : '张胡斌','age' : '你猜','性别' : '你猜','身高':'你猜' }
        # print(dic ['name'])
    # 字典名[键名]查看对应的值（没有的话会报错）
        # dic = { 'name' : '张胡斌','age' : '你猜','性别' : '你猜','身高':'你猜' }
        # a = dic.get('name')
        # print(a)
    #.get根据键来查看值，如果不存在默认会返回None，同样也可以设置返回值
        # dic = { 'name' : '张胡斌','age' : '你猜','性别' : '你猜','身高':'你猜' }
        # a = dic.keys()
        # b = dic.values()
        # c = dic.items()
        # print( a )
        # print( b )
        # print( c)
    # .keys() .values() .items() 类似于列表的容器(键，值，键值对)，不可以使用索引
    #可以通过list()转换成列表，可以使用for循环
# dict.formkeys(参数1,参数2 ) 用参数1的值作为键,参数2的值作为值创建一个新字典
# enumerate()枚举，
