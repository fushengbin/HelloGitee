# 列表的索引步长和str一样，切片也是
# 列表的增删改查：
#         zhang = ['zhangbaocheng','zhangdabiao','gongzhangguyuewenwu']
#         zhang.append('xiaomage')
#         print(zhang)
#     .append()给一个列表追加一个参数（就是在末尾）
#         zhang = ['zhangbaocheng','zhangdabiao','gongzhangguyuewenwu']
#         zhang.insert(1,'xiaomage')
#         print(zhang)
#     .insert()给一个列表插入一个参数（跟索引和插入的东西）
#         zhang = ['zhangbaocheng','zhangdabiao','gongzhangguyuewenwu']
#         zhang1 = ['buzhidao','xiangshuoshenm']
#         zhang.extend(zhang1)
#         zhang.extend('xiaomage')
#         zhang += zhang1
#         print(zhang)
#     .extend()可迭代增加，说白了就是把你输入的东西全部加进去，中间可以是列表，也就是将zhang1列表中的东西全部加到列表zhang里面,还有一个不同的写法zhang+=zhang1,效果是一样的
#         zhang = ['zhangbaocheng','zhangdabiao','gongzhangguyuewenwu']
#         zhang.pop(0)
#         print(zhang)
#     .pop()按照索引删除对象，有返回值，返回值就是你删除的那个东西
#         zhang = ['zhangbaocheng','zhangdabiao','gongzhangguyuewenwu']
#         zhang.remove('zhangbaocheng')
#         print(zhang)
#     .remove()按照元素去删除
#         zhang = ['zhangbaocheng','zhangdabiao','gongzhangguyuewenwu']
#         zhang.clear()
#         print(zhang)
#     .clear()清空列表
#         zhang = ['zhangbaocheng','zhangdabiao','gongzhangguyuewenwu']
#         del zhang[1]
#         print(zhang)
#         del zhang[::2]
#         print(zhang)
#     del 删除，可以按照索引和切片删除，
#         zhang = ['zhangbaocheng','zhangdabiao','gongzhangguyuewenwu']
#         zhang[ 0 ] = 'yiersan'
#         print(zhang)
#     列表名+[索引] = 字符串之类的（或者其它你想改的东西） 改列表内容 （每次只能改一个）
#         zhang = ['zhangbaocheng','zhangdabiao','gongzhangguyuewenwu']
#         zhang[:] = 'yiersan'
#         print(zhang)
#     列表名 + 切片 = 字符串之类的（或者其它你想改的东西）  改列表内容
#         zhang = ['zhangbaocheng','zhangdabiao','gongzhangguyuewenwu']
#         zhang[::2] = 'zh'
#         print(zhang)
#     列表名 + 切片 = 字符串之类的（或者其它你想改的东西） 改列表内容，但是这个有长度限制，
#         zhang = ['zhangbaocheng','zhangdabiao','gongzhangguyuewenwu','zhangdabiao']
#         print(zhang.count('zhangdabiao'))
#     .count()和字符串里面的count一样，用来计算列表中字符串出现的次数
        # zhang = ['zhangbaocheng','zhangdabiao','gongzhangguyuewenwu','zhangdabiao']
        # print(zhang.index('zhangdabiao'))
#     .index（）和字符串里面的index一样，用来查找改字符串的索引，也可以切片
#         shuzi['3712','321313','21321','312','4','55','65','54','6','56']
#         shuzi.sort()
#         shuzi.sort(reverse=True)
#         print(shuzi)
#     .sort()将数字由小到大排序(默认)，可以在里面添加参数reverse=True让其由大到小
#         shuzi = ['3712','321313','21321','312','4','55','65','54','6','56']
#         shuzi.reverse()
#         print(shuzi)
#     .reverse()将列表里面内容翻转过来（也就是倒过来）
#         shuzi = ['3712','321313','21321','312','4','55','65','54','6','56']
#         print(len(shuzi))
#     len()和字符串里面的一样，测量里面一共有多少个元素/字符串/或者其它组成部分
#         z = ['sdadisaojd','dsadsadads','dafqwqfgfeds']
#         z[0] = z [0].upper()
#         print(z)
#     .upper()/lower()和字符串里面的一样，将某个元素改成大/小写
#         l = range(1,6)
#         print(l)
#         for i in l:
#         print(i)
#     range和列表基本一样，但是不能直接通过print来看，需要使用for循环来看，个人觉得吧，更像是一个集合
#     在列表能运用的东西在range里面同样可以，比如切片，索引等
# 使用jion可以将列表种的元素连接形成字符串，前提必须全是字符串，jion(中间可以加你想使用的连接符)
#         l1 = ['zhang','hu','bin']
#         l1 *= 2
    # list*= count，将该列表中的数量重复多少次，整数

    


