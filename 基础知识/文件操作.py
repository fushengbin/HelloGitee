# global 声明这个是一个全局变量
#语法格式： open('文件名'，mode（模式）= 'r'(r:read(只读),w：write（只写,打开的时候会清空原始数据,如果没有则创建一个新的,一般用于新建文件），a：append（只能添加））,encoding='编码格式'(以什么编码格式读取))默认是读取当前文件夹里面的某个文件，及当你读取的文件在该程序目录下就不用跟文件目录
# 读取文件：
'''
# 打开文件:
file_object = open('1.txt',mode='r',encoding='utf-8')
# 读取文件(就是.read（）)
print(file_object.read())
# # 关闭文件（就是.close()）
file_object.close()
'''
# 写入文件(会改变原始文件里面的数据):
'''#打开文件:
file_object = open('1.txt',mode='w',encoding='utf-8')
# 写入文件：
file_object.write('chl')
# 关闭文件
file_object.close()'''
#添加文件
'''#打开文件:
file_object = open('1.txt',mode='a',encoding='utf-8')
# 写入文件：
file_object.write('chl')
# 关闭文件
file_object.close()'''
# 可读可写
'''#打开文件:
file_object = open('1.txt',mode='r+',encoding='utf-8')
file_object.seek(数字)#移动光标到第几个字节(默认在最左边)
# 写入文件：
file_object.write('chl')#默认在文件之前添加,可以自己确认在那个自己添加
# 关闭文件
file_object.close()'''
# 可写可读
'''#打开文件:
file_object = open('1.txt',mode='w+',encoding='utf-8')
# 写入文件：
file_object.write('chl')
# 查看文件:
print(file_object.read())
# 关闭文件
file_object.close()'''
# 可追加可读
'''#打开文件:
file_object = open('1.txt',mode='a+',encoding='utf-8')
# 移动光标(因为是追加,所以说需要把光标移动到最前面,否则就是在最后面,查看出来就什么都没有了)：
file_object.seek(0)
# 查看文件:
print(file_object.read())
# 添加文件(只会追加,然后无论光标的位置在哪,):
file_object.write('6666')
# 关闭文件
file_object.close()'''
# 一行一行读取:
'''# 打开文件:
file_object = open('1.txt',mode='r',encoding='utf-8')
# 读取文件(就是.read（）)
print(file_object.readlines())
# 关闭文件（就是.close()）
file_object.close()'''
file_object = open('1.txt',mode='r',encoding='utf-8')
print(file_object)
a = file_object.readlines()
print(a)
b = a[0]
print(b)