#     索引与切片
#             z1 = '张宝成不晓得多少天学习python路程'
#         索引从左到右，从零开始（从右到左是-1开始越来越小）,
#             z2 = z1[0]
#             print(z2)
#         切片取值：[起始索引:结尾索引+1:步长]顾头不顾尾
#             print(z1[3:9])*
#         起始索引是0，可以写成[:结尾索引+1]
#             print(z1[3:])
#         同理，取到最后一位可以写成[起始索引:]
#             print(z1[-1:-7])
#         反向取值一定要加反向步长
#             print(z1[-1:-7:-1])
# 字符串的常用操作方法
#     对字符串的操作都会形成一个新的字符串与原字符串没有任何关系（不会改变原字符串）
#         z = 'ZhangBaoChen'
#         print(z.capitalize())
#     .capitalize 首字母大写，---其余字母小写
# z = 'dsadadsa'
# print(z.swapcase())
#     .swapcase()(大小写反转)
#         print(z.center(20,'😀'))
#     .center()中间跟数字，将某个字符串，用空格（默认是空格，可以通过在数字后面添加，字符串来选择其它的东西）填充到你输入的那个数字的那么多个，并将原字符串居中，
#         z1 = 'zhang bao cheng'
#         print(z1.title())
#     .title() 将字符串的非字母元素隔开的每个单词的首字母大写
#         z1 = 'dsaadadsfasd'
#         print(z1.upper())
#         print(z1.lower())
#     .upper()/.lower() 将该字符串全部变大写/小写
#         print(z.startswith('z'))
#         print(z.endswith('n'))
#     .startswith()/endswith()判断是否以该字符/字符串开始/结尾（判断多个字符串的时候，必须是连在一起的）
#     同样也可以判断该字符串是不是这个字符串中某个索引的开头的，这样的话就必须在后面加上索引，如
#         print(z.startswith('B',5))
#     z = 'Zhang Bao Chen '
#         print(z.find('F'))
#     .find()判断该字符串的索引是几，如果是连续的字符串，则就显示第一个的索引(.index()功能与.find()基本一致，只是没有查找的字符串的时候会报错)
#         print(z.find('h'))
#     当判断一个出现多次的字符串的时候，就只会显示出现的第一次的那个，这个时候这里面的切片就有用了，我们来看看
#         print(z.find('h',5,-2))
#     .strip()默认去除字符串前后的换行符，制表符，空格等，
#         print(z.strip('Z'))
#     同理可以只去除前面或者后面的空格，字符串啊什么的，就有另外两个用法：
#     .lstrip()和.rstrip()只去除前面/后面空格
#         print(z.split())
#     .split()将字符串改变成列表，默认是以空格为分割符，可以指定以什么为分割符，也可以指定分割次数，默认是全部(从左到右)，同理就有从右到左的.rsplit()
#         zi = 'zhb是个人才，zhb很牛逼是不可能的，zhb很善良'
#         print(zi.replace('zhb','liubo'))
#     .replace() 从左到右将xx用xx替换，可以设置次数
#         zi = 'zhanghubin'
#         print('='.join(zi))
#     .join()将字符串中间用你前面给出的那个字符连接起来（注意，用法和其它的有点不同，它是前面跟连接的字符，后面跟对象）
#         zi = ['zhang','hu','bin']
#     .join()还有一个常用的功能就是将list（列表）转化成str（字符串）前提是列表中的每个元素都是字符串
#     .format():和%s差不多,一共有三种用法(格式化输出)
#         第一种(em,就%d?):
#             zi = '我叫{},今年{},喜欢{}'
#             print(zi.format('zhb','17','chl')).
#             自我总结,这个和%s用法一只
#         第二种(序列号):
#             zi = '我叫{0},今年{1},喜欢{2},那个傻fufu的{2}'
#             print(zi.format('zhb','17','chl'))
#             这个就和%d不一样了,然后个人觉得应该在程序的处理中,有多次出现的变量的就应该用这种方法
#         第三种(键值对):
#             zi = '我叫{me},今年{age},喜欢{she},那个傻fufu的{she}'
#             print(zi.format(me = 'zhb', age = '17',she = 'chl'))
#             这个的话,感觉就是和字典一起运用,然后不用管顺序的问题
#     is系列
#             name = 'zhang123'
#             print(name.isalnum())
#         .isalnum()判断该字符串是不是以字母和数字组成
#             name = 'dhsaj'
#             print(name.isalpha())
#         .isalpha()判断该字符串是不是只由字母组成
#             name = '1231'
#             print(name.isdigit())
#         .isdigit()判断该字符串是不是只由数字组成
        # name = '12.31'
        # print(name.isdigit())
#         .isdecimal()判断该字符串是不是由整数组成
#             a = 'dsnakdkasamda'
# #             print(a.count('a'))
#         .count()计算某个字符串中,某个字符/串出现的次数，也可以给定范围，但是不能一次计算多个
#             name = '1231'
#             print(len(name))
#         len()计算字符串里面有多少个字符
#     for循环
#         for + 变量 in iterable（可迭代对象）
#             循环体
#     for循环可以跟break，continue，同样也可以加if else
#     .isspace查看这个字符串是不是空的(换行符什么的也行)
print(a)




