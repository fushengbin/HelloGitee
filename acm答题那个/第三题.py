#!/usr/bin/env python
# -*- coding:utf-8 -*-
while True:
    num = input("").strip()
    if num.isdigit():
        num = int(num)
        if 0 < num <= 10000:
            if num % 2 != 0 and num % 3 != 0 and num % 5 != 0:
                print("null")
                break
            else:
                for i in range(num+1):
                    if i == 0:
                        pass
                    else:
                        if i % 2 == 0 or i % 3 == 0 or i % 5 == 0:
                            print(i)
                break
        else:
            print("Please enter a number between 0 and 10000")
    else:
        print("Please enter an integer!")
