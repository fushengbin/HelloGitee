#!/usr/bin/env python
# -*- coding:utf-8 -*-
while True:
    num = input("").strip()
    if num.isdigit():
        num = int(num)
        if 1 <= num <= 100:
            print(" "*(num-1), end="")
            print(0)
            RangeTime = num
            for i in range(num):
                if i == 0:
                    pass
                else:
                    print(" "*(num-1-i), end="")
                    print("* "*(i+1))
            break
        else:
            print("Too many layers")
    else:
        print("Please enter an integer!")

# 注意acm机制是输入描述和输出描述就必须是它给定的那种，也就是里面输出描述和输入描述一样的
