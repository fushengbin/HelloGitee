#!/usr/bin/env python 
#-*- coding:utf-8 -*-
import datetime
from read_data import *
from login import *
TeacherName = []  # 老师姓名缩写
TeacherSign = [] #当前签到老师
def StduentChoose(): #老师进行选择操作
    while True:
        print('请输入您要选择的操作：\n\t1.签到\t2.修改密码\t3.退出')
        Choose = input('>>>').strip()
        if Choose == '1' or Choose == '签到':
            return 1
        elif Choose == '2' or Choose == '修改密码':
            return 2
        elif Choose == '3' or Choose.lower() == 'b' or Choose == '退出':
            return 3
        else:
            print('输入有误！')
def ChangePassWd(UserName): #修改密码
    while True:
        PassWd = PasswdNoSee('请输入当前密码：')
        if PassWd == StudentPassWd[StudentName.index(UserName)]:
            while True:
                print('\n')
                PassWd1 = PasswdNoSee('请输入新密码：')
                print('\n')
                PassWd2 = PasswdNoSee('请再次输入密码：')
                if PassWd1 == PassWd2:
                    print('\n您已成功修改密码！')
                    StudentPassWd[StudentName.index(UserName)] = PassWd2
                    with open('Students.txt',encoding='utf-8',mode='w') as f:
                        for i in StudentName:
                            f.write(('%s：%s；%s\n')%(i,StudentNum[StudentName.index(i)],StudentPassWd[StudentName.index(i)]))
                    return
                else:
                    print('\n')
                    print('两次密码不相等')
        else:
            print('\n密码不正确！')
def ReadTeach():#判断那些老师现在在签到
    if QxzTime != []: #如果老师签到时间不为空
        TeacherName.append('qxz')   #添加老师缩写状态
        TeacherSign.append('祁相志') #老师签到状态添加该老师
    elif ZhyTime != []:
        TeacherName.append('zhy')
        TeacherSign.append('赵侯宇')
    elif NymTime != []:
        TeacherName.append('nym')
        TeacherSign.append('牛彦敏')
    elif DzgTime != []:
        TeacherName.append('dzg')
        TeacherSign.append('戴政国')
    elif FjyTime != []:
        TeacherName.append('fjy')
        TeacherSign.append('范金艳')
    elif QxzTime == [] and ZhyTime == [] and NymTime == [] and DzgTime == [] and FjyTime == []:
        return '当前没有老师签到'
    return TeacherName,TeacherSign
def SignIn(UserName):
    print('当前以下老师正在签到：')
    for i in TeacherSign:
        print('\t%d.%s'%(TeacherSign.index(i),i) ,end = '') #打印正在签到老师的名单
    while True:
        print('\n输入编号或者姓名缩写以签到')
        UserChoose = input('>>>').strip()
        if UserChoose.isdigit():   #是输入的编号时
            UserChoose = int(UserChoose)
            if UserChoose <= len(TeacherSign)-1: #判断编号是不是存在
                Time =  datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')  #获取当前时间
                if TeacherSign[UserChoose] == '祁相志':
                    if UserName not in QxzPeople:
                        if QxzTime[0] < Time < QxzTime[1]:  #判断时间是不是在当前时间段
                            Num = int(QxzNum) + 1
                            QxzPeople.append(UserName)
                            QxzStTime.append(Time)
                            with open('Qxz.txt',mode='w',encoding='utf-8') as f:
                                f.write(('%s,%s\n')%(QxzTime[0],QxzTime[1]))
                                for i in QxzPeople:
                                    f.write(('%s ')%i)
                                f.write(('\n%s\n') % Num)
                                for i in QxzStTime:
                                    f.write(( '*%s' )% i)
                            print('签到成功')
                            return
                        else:
                            print("未在签到时间段内")
                            return False
                    else:
                        print('您已签到，无需重复签到！')
                        return
                elif TeacherSign[UserChoose] == '赵侯宇':
                    if UserName not in ZhyPeople:
                        if ZhyTime[0] < Time < ZhyTime[1]:  # 判断时间是不是在当前时间段
                            Num = int(ZhyNum) + 1
                            ZhyPeople.append(UserName)
                            ZhyStTime.append(Time)
                            with open('Zhy.txt', mode='w', encoding='utf-8') as f:
                                f.write(('%s,%s\n') % (ZhyTime[0], ZhyTime[1]))
                                for i in ZhyPeople:
                                    f.write(('%s ') % i)
                                f.write(('\n%s\n') % Num)
                                for i in ZhyStTime:
                                    f.write(('*%s') % i)
                            print('签到成功')
                            return
                        else:
                            print("未在签到时间段内")
                            return False
                    else:
                        print('您已签到，无需重复签到！')
                        return
                elif TeacherSign[UserChoose] == '牛彦敏':
                    if UserName not in NymPeople:
                        if NymTime[0] < Time < NymTime[1]:  # 判断时间是不是在当前时间段
                            Num = int(NymNum) + 1
                            NymPeople.append(UserName)
                            NymStTime.append(Time)
                            with open('Nym.txt', mode='w', encoding='utf-8') as f:
                                f.write(('%s,%s\n') % (NymTime[0], NymTime[1]))
                                for i in NymPeople:
                                    f.write(('%s ') % i)
                                f.write(('\n%s\n') % Num)
                                for i in NymStTime:
                                    f.write(('*%s') % i)
                            print('签到成功')
                            return
                        else:
                            print("未在签到时间段内")
                            return False
                    else:
                        print('您已签到，无需重复签到！')
                        return
                elif TeacherSign[UserChoose] == '戴政国':
                    if UserName not in DzgPeople:
                        if DzgTime[0] < Time < DzgTime[1]:  # 判断时间是不是在当前时间段
                            Num = int(DzgNum) + 1
                            DzgPeople.append(UserName)
                            DzgStTime.append(Time)
                            with open('Dzg.txt', mode='w', encoding='utf-8') as f:
                                f.write(('%s,%s\n') % (DzgTime[0], DzgTime[1]))
                                for i in DzgPeople:
                                    f.write(('%s ') % i)
                                f.write(('\n%s\n') % Num)
                                for i in DzgStTime:
                                    f.write(('*%s') % i)
                            print('签到成功')
                            return
                        else:
                            print("未在签到时间段内")
                            return False
                    else:
                        print('您已签到，无需重复签到！')
                        return
                elif TeacherSign[UserChoose] == '范金艳':
                    if UserName not in FjyPeople:
                        if FjyTime[0] < Time < FjyTime[1]:  # 判断时间是不是在当前时间段
                            Num = FjyNum + 1
                            FjyPeople.append(UserName)
                            FjyStTime.append(Time)
                            with open('Dzg.txt', mode='w', encoding='utf-8') as f:
                                f.write(('%s,%s\n') % (FjyTime[0], FjyTime[1]))
                                for i in FjyPeople:
                                    f.write(('%s ') % i)
                                f.write(('\n%s\n') % Num)
                                for i in FjyStTime:
                                    f.write(('%s') % i)
                            print('签到成功')
                            return
                        else:
                            print("未在签到时间内")
                            return False
                    else:
                        print('您已签到，无需重复签到！')
                        return
                else:
                    print('请输入正确的编号')
            else:
                print('请输入正确数字')
        elif UserChoose.lower() in TeacherName:  #输入姓名缩写时
            Time = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
            if UserChoose.lower() == 'qxz':
                if UserName not in QxzPeople:
                    if QxzTime[0] < Time < QxzTime[1]:  # 判断时间是不是在当前时间段
                        Num = int(QxzNum) + 1
                        QxzPeople.append(UserName)
                        QxzStTime.append(Time)
                        with open('Qxz.txt', mode='w', encoding='utf-8') as f:
                            f.write(('%s,%s\n') % (QxzTime[0], QxzTime[1]))
                            for i in QxzPeople:
                                f.write(('%s ') % i)
                            f.write(('\n%s\n') % Num)
                            for i in QxzStTime:
                                f.write(('*%s') % i)
                        print('签到成功')
                        return
                    else:
                        print("未在签到时间段内")
                        return False
                else:
                    print('您已签到，无需重复签到！')
                    return
            elif UserChoose.lower() == 'zhy':
                if UserName not in ZhyPeople:
                    if ZhyTime[0] < Time < ZhyTime[1]:  # 判断时间是不是在当前时间段
                        Num = int(ZhyNum) + 1
                        ZhyPeople.append(UserName)
                        ZhyStTime.append(Time)
                        with open('Zhy.txt', mode='w', encoding='utf-8') as f:
                            f.write(('%s,%s\n') % (ZhyTime[0], ZhyTime[1]))
                            for i in ZhyPeople:
                                f.write(('%s ') % i)
                            f.write(('\n%s\n') % Num)
                            for i in ZhyStTime:
                                f.write(('*%s') % i)
                        print("签到成功")
                        return
                    else:
                        print("未在签到时间段内")
                        return False
                else:
                    print('您已签到，无需重复签到！')
                    return
            elif UserChoose.lower() == 'nym':
                if UserName not in NymPeople:
                    if NymTime[0] < Time < NymTime[1]:  # 判断时间是不是在当前时间段
                        Num = int(NymNum) + 1
                        NymPeople.append(UserName)
                        NymStTime.append(Time)
                        with open('Nym.txt', mode='w', encoding='utf-8') as f:
                            f.write(('%s,%s\n') % (NymTime[0], NymTime[1]))
                            for i in NymPeople:
                                f.write(('%s ') % i)
                            f.write(('\n%s\n') % Num)
                            for i in NymStTime:
                                f.write(('*%s') % i)
                        print("签到成功")
                        return
                    else:
                        print("未在签到时间段内")
                        return False
                else:
                    print('您已签到，无需重复签到！')
                    return
            elif UserChoose.lower() == 'dzg':
                if UserName not in DzgPeople:
                    if DzgTime[0] < Time < DzgTime[1]:  # 判断时间是不是在当前时间段
                        Num = int(DzgNum) + 1
                        DzgPeople.append(UserName)
                        DzgStTime.append(Time)
                        with open('Dzg.txt', mode='w', encoding='utf-8') as f:
                            f.write(('%s,%s\n') % (DzgTime[0], DzgTime[1]))
                            for i in DzgPeople:
                                f.write(('%s ') % i)
                            f.write(('\n%s\n') % Num)
                            for i in DzgStTime:
                                f.write(('*%s') % i)
                        print("签到成功")
                        return
                    else:
                        print("未在签到时间段内")
                        return False
                else:
                    print('您已签到，无需重复签到！')
                    return
            elif UserChoose.lower() == 'fjy':
                if UserName not in FjyPeople:
                    if FjyTime[0] < Time < FjyTime[1]:  # 判断时间是不是在当前时间段
                        Num = int(FjyNum) + 1
                        FjyPeople.append(UserName)
                        FjyStTime.append(Time)
                        with open('Dzg.txt', mode='w', encoding='utf-8') as f:
                            f.write(('%s,%s\n') % (FjyTime[0], FjyTime[1]))
                            for i in FjyPeople:
                                f.write(('%s ') % i)
                            f.write(('\n%s\n') % Num)
                            for i in FjyStTime:
                                f.write(('*%s') % i)
                        print("签到成功")
                        return
                    else:
                        return False
                else:
                    print('您已签到，无需重复签到！')
                    return
            else:
                print('请输入正确缩写！')
        else:
            print('输入有误')
def Student(UserName):
    print('\n')
    print(('欢迎学生%s' % UserName).center(50, '*'))
    Rseult = ReadTeach()
    if len(Rseult) == 2:
        while True:
            Rseult = StduentChoose()
            if Rseult == 1:
                SignIn(UserName)
            elif  Rseult == 2:
                ChangePassWd(UserName)
            elif Rseult == 3:
                exit()
    else:
        while True:
            Rseult = StduentChoose()
            if Rseult == 1:
                print('当前没有老师签到！')
            elif Rseult == 2:
                ChangePassWd(UserName)
            elif Rseult == 3:
                exit()
