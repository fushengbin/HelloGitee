import os
QxzTime = []  #xx老师签到时间段
QxzPeople = [] #xx老师已签到人
QxzNum = 0  #xx老师已签到人数
QxzStTime = [] #xx老师已签到时间（学生签到的时间）
DzgTime = []
DzgPeople = []
DzgNum = 0
DzgStTime =[]
NymTime = []
NymPeople = []
NymNum = []
NymStTime = []
ZhyTime = []
ZhyPeople = []
ZhyNum = 0
ZhyStTime = []
FjyTime = []
FjyPeople = []
FjyNum = 0
FjyStTime  = []
def rst_data():#读取学生学号密码数据，并且变成一个关于三个列表的元组输出
    xm_list = []
    xh_list = []
    pw_list = []
    with open("Students.txt", "r", encoding="utf-8") as f:
        for i in f:
            xm = i[:i.find("：")]
            xh = i[i.find("：") + 1:i.find("；")]
            pw = i[i.find("；") + 1:].strip()
            xm_list.append(xm)
            xh_list.append(xh)
            pw_list.append(pw)
        return xm_list,xh_list,pw_list

def rsun_data():#读取孙的账号密码数据
    with open("Sun.txt","r",encoding="utf-8") as f:
        r = f.readline()
        xm = r[:r.find("：")]
        pw = r[r.find("：")+1:].strip()
        return xm,pw

def rth_data():#读取任课老师的账号密码
    xm_list = []
    pw_list = []
    with open("TeacherList.txt","r",encoding="utf-8") as f:
        for i in f:
            xm = i[:i.find(":")]
            pw = i[i.find(":")+1:].strip()
            xm_list.append(xm)
            pw_list.append(pw)
    return xm_list,pw_list

def kq_time(x):#单个老师的签到情况
    a = os.path.exists("{}.txt".format(x))
    if a == True:
        with open("{}.txt".format(x),"r",encoding="utf-8") as f1:
            People = []
            QdTime = []
            Time0 = f1.readline()
            People = f1.readline().strip()
            People = People.split(' ')
            Num = f1.readline().strip()
            if Num == '':
                Num = 0
            QdTime = f1.readline().strip()
            QdTime = QdTime.split('*')
            QdTime.pop(0)
            Time = []
            Time.append(Time0[:Time0.find(",")])
            Time.append(Time0[Time0.find(",")+1:].strip())
            return Time,People,Num,QdTime
    else:
        return
    
Students_Kq_List = rst_data()
StudentName = Students_Kq_List[0]
StudentNum = Students_Kq_List[1]
StudentPassWd = Students_Kq_List[2]
sun = rsun_data()
Sun = sun[0]
SunPassWd = sun[1]
Teather_Kq_List = rth_data()
TeacherName = Teather_Kq_List[0]
TeacherPassWd = Teather_Kq_List[1]
Qxz_Kq_List = kq_time("Qxz")
if Qxz_Kq_List :
    QxzTime = Qxz_Kq_List[0]
    if len(Qxz_Kq_List) > 2:
        QxzPeople = Qxz_Kq_List[1]
        QxzNum = Qxz_Kq_List[2]
        QxzStTime = Qxz_Kq_List[3]
Nym_Kq_List = kq_time("Nym")
if Nym_Kq_List :
    NymTime = Nym_Kq_List[0]
    if len(Nym_Kq_List)>2:
        NymPeople = Nym_Kq_List[1]
        NymNum = Nym_Kq_List[2]
        NymStTime = Nym_Kq_List[3]
Dzg_Kq_List = kq_time("Dzg")
if Dzg_Kq_List:
    DzgTime = Dzg_Kq_List[0]
    if len(Dzg_Kq_List)>2:
        DzgPeople = Dzg_Kq_List[1]
        DzgNum = Dzg_Kq_List[2]
        DzgStTime = Dzg_Kq_List[3]
Zhy_Kq_List = kq_time("Zhy")
if Zhy_Kq_List:
    ZhyTime = Zhy_Kq_List[0]
    if len(Zhy_Kq_List)>2:
        ZhyPeople = Zhy_Kq_List[1]
        ZhyNum = Zhy_Kq_List[2]
        ZhyStTime = Zhy_Kq_List[3]
Fjy_Kq_List = kq_time("Fjy")
if Fjy_Kq_List:
    FjyTime = Fjy_Kq_List[0]
    if len(FjyTime)>2:
        FjyPeople = Fjy_Kq_List[1]
        FjyNum = Fjy_Kq_List[2]
        FjyStTime = Fjy_Kq_List[3]
