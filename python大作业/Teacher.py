#!/usr/bin/env python 
#-*- coding:utf-8 -*-
import datetime
import os
from prettytable import PrettyTable
from read_data import *
from login import *
def Judge(UserName): #将老师名字改成缩写，方便后面的操作
    if UserName == '祁相志':
        Name = 'Qxz'
        return Name
    elif UserName == '戴政国':
        Name = 'Dzg'
        return Name
    elif UserName == '牛彦敏':
        Name = 'Nym'
        return Name
    elif UserName == '赵侯宇':
        Name = 'Zhy'
        return Name
    elif UserName == '范金艳':
        Name = 'Fjy'
        return Name
def TeacherChoose(): #老师进行选择操作
    while True:
        print('请输入您要选择的操作：\n\t1.签到\t2.修改密码\t3.查询\t4.退出')
        Choose = input('>>>').strip()
        if Choose == '1' or Choose == '签到':
            return 1
        elif Choose == '2' or Choose == '修改密码':
            return 2
        elif Choose == '3' or Choose == '查询':
            return 3
        elif Choose == '4' or Choose.lower() == 'b' or Choose == '退出':
            return 4
        else:
            print('输入有误！')
def PasswdNoSee(a):#密码不可见
    import msvcrt, os
    print(a, end='', flush=True)
    li = []
    while 1:
        ch = msvcrt.getch()
        #回车
        if ch == b'\r':
            return b''.join(li).decode() #把list转换为字符串返回
            break
        #退格
        elif ch == b'\x08':
            if li:
                li.pop()
                msvcrt.putch(b'\b')
                msvcrt.putch(b' ')
                msvcrt.putch(b'\b')
        #Esc
        elif ch == b'\x1b':
            break
        else:
            li.append(ch)
            msvcrt.putch(b'*')
    return b''.join(li).decode()##
def ChangePassWd(UserName): #修改密码3
    while True:
        PassWd = PasswdNoSee('请输入当前密码：')
        if PassWd == TeacherPassWd[TeacherName.index(UserName)]:
            while True:
                print('\n')
                PassWd1 = PasswdNoSee('请输入新密码：')
                print('\n')
                PassWd2 = PasswdNoSee('请再次输入密码：')
                if PassWd1 == PassWd2:
                    print('\n您已成功修改密码！')
                    TeacherPassWd[TeacherName.index(UserName)] = PassWd2
                    with open('TeacherList.txt',encoding='utf-8',mode='w') as f:
                        for i in TeacherName:
                            f.write(('%s,%s\n')%(i,TeacherPassWd[TeacherName.index(i)]))
                    return
                else:
                    print('\n')
                    print('两次密码不相等')
        else:
            print('\n密码不正确！')
def Sign(Name): #发布签到以及提前结束
    if Name == 'Qxz':
        if len(QxzTime) > 0 :
            while True:
                print('是否提前结束？(y/n)')
                Choose = input('>>>').strip()
                if Choose == '是' or Choose.lower() == 'y':
                    Time = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                    QxzTime.pop()
                    QxzTime.append(Time)
                    with open('Qxz.txt',encoding='utf-8',mode='w') as f:
                        f.write(("%s,%s\n")%([0],[1]))
                        f.write(QxzNum)
                        f.write('\n')
                        for i in QxzName:
                            f.write(i)
                        f.write('\n')
                        for i in QxzStTime:
                            f.write(i)
                    print('当前签到时间段为%s-%s')
                    return
                elif Choose == '否' or Choose.lower() == 'n':
                    print('可签到时间不变')
                    return
                else:
                    print('输入有误！')
        else:
            while True:
                print('您还未开始签到是否开始？(y/n)')
                Choose = input('>>>').strip()
                if Choose == '是' or Choose.lower() == 'y':
                    while True:
                        print('是否输入结束时间？（y/n）')
                        Choose = input('>>>').strip()
                        if  Choose.lower() == 'y'  or Choose == '是':
                            while True:
                                print('请输入多久分钟后结束？')
                                Choose = input('>>>').strip()
                                if Choose.isdigit():
                                    Choose = int(Choose)
                                    StartTime = datetime.datetime.now()
                                    StopTime = (StartTime + datetime.timedelta(minutes=Choose)).strftime('%Y-%m-%d %H:%M:%S')
                                    StartTime = StartTime.strftime('%Y-%m-%d %H:%M:%S')
                                    with open('Qxz.txt', encoding='utf-8', mode='w') as f:
                                        f.write(("%s,%s\n") % (StartTime, StopTime))
                                    print(('当前签到时间为%s---%s')%(StartTime, StopTime))
                                    return
                                else:
                                    print('请输入正确数字！')
                        elif Choose.lower() == 'n' or Choose == '否':
                            StartTime = datetime.datetime.now()
                            StopTime = (StartTime + datetime.timedelta(minutes=10)).strftime('%Y-%m-%d %H:%M:%S')
                            StartTime = StartTime.strftime('%Y-%m-%d %H:%M:%S')
                            with open('Qxz.txt', encoding='utf-8', mode='w') as f:
                                f.write(("%s,%s\n") % (StartTime,  StopTime))
                            print(('当前签到时间为%s---%s') % (StartTime, StopTime))
                            return
                elif Choose == '否' or Choose.lower() == 'n':
                    print('那就只能退出了')
                    exit()
                else:
                    print('输入有误！！')
    elif Name ==  'Dzg':
        if len(DzgTime) >0:
            while True:
                print('是否提前结束？(y/n)')
                Choose = input('>>>').strip()
                if Choose == '是' or Choose.lower() == 'y':
                    Time = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                    DzgTime.pop()
                    DzgTime.append(Time)
                    with open('Dzg.txt',encoding='utf-8',mode='w') as f:
                        f.write(("%s,%s\n")%(DzgTime[0],DzgTime[1]))
                        f.write(DzgNum)
                        f.write('\n')
                        for i in DzgName:
                            f.write(i)
                        f.write('\n')
                        for i in DzgStTime:
                            f.write(i)
                    return
                elif Choose == '否' or Choose.lower() == 'n':
                    print('签到时间不变')
                    return
                else:
                    print('输入有误！')
        else:
            while True:
                print('您还未开始签到是否开始？(y/n)')
                Choose = input('>>>').strip()
                if Choose == '是' or Choose.lower() == 'y':
                    while True:
                        print('是否输入结束时间？（y/n）')
                        Choose = input('>>>').strip()
                        if  Choose.lower() == 'y'  or Choose == '是':
                            while True:
                                print('请输入多久分钟后结束？')
                                Choose = input('>>>').strip()
                                if Choose.isdigit():
                                    Choose = int(Choose)
                                    StartTime = datetime.datetime.now()
                                    StopTime = (StartTime + datetime.timedelta(minutes=Choose)).strftime('%Y-%m-%d %H:%M:%S')
                                    StartTime = StopTime.strftime('%Y-%m-%d %H:%M:%S')
                                    with open('Dzg.txt', encoding='utf-8', mode='w') as f:
                                        f.write(("%s,%s\n") % (StartTime, StopTime))
                                    print(('当前签到时间为%s---%s') % (StartTime, StopTime))
                                    return
                                else:
                                    print('请输入正确数字！')
                        elif Choose.lower() == 'n' or Choose == '否':
                            StartTime = datetime.datetime.now()
                            StopTime = (StartTime + datetime.timedelta(minutes=10)).strftime('%Y-%m-%d %H:%M:%S')
                            StartTime = StartTime.strftime('%Y-%m-%d %H:%M:%S')
                            with open('Dzg.txt', encoding='utf-8', mode='w') as f:
                                f.write(("%s,%s\n") % (StartTime, StopTime))
                            print(('当前签到时间为%s---%s') % (StartTime, StopTime))
                            return
                elif Choose == '否' or Choose.lower() == 'n':
                    print('那就只能退出了')
                    exit()
                else:
                    print('输入有误！！')
    elif Name ==  'Zhy':
        if len(ZhyTime)>0:
            while True:
                print('是否提前结束？(y/n)')
                Choose = input('>>>').strip()
                if Choose == '是' or Choose.lower() == 'y':
                    Time = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                    ZhyTime.pop()
                    ZhyTime.append(Time)
                    with open('Zhy.txt',encoding='utf-8',mode='w') as f:
                        f.write(("%s,%s\n")%(ZhyTime[0],ZhyTime[1]))
                        f.write(ZhyNum)
                        f.write('\n')
                        for i in ZhyName:
                            f.write(i)
                        f.write('\n')
                        for i in ZhyStTime:
                            f.write(i)
                    return
                elif Choose == '否' or Choose.lower() == 'n':
                    return
                else:
                    print('输入有误！')
        else:
            while True:
                print('您还未开始签到是否开始？(y/n)')
                Choose = input('>>>').strip()
                if Choose == '是' or Choose.lower() == 'y':
                    while True:
                        print('是否输入结束时间？（y/n）')
                        Choose = input('>>>').strip()
                        if  Choose.lower() == 'y'  or Choose == '是':
                            while True:
                                print('请输入多久分钟后结束？')
                                Choose = input('>>>').strip()
                                if Choose.isdigit():
                                    Choose = int(Choose)
                                    StartTime = datetime.datetime.now()
                                    StopTime = (StartTime + datetime.timedelta(minutes=Choose)).strftime('%Y-%m-%d %H:%M:%S')
                                    StartTime = StartTime.strftime('%Y-%m-%d %H:%M:%S')
                                    with open('Zhy.txt', encoding='utf-8', mode='w') as f:
                                        f.write(("%s,%s\n") % (StartTime, StopTime))
                                    print(('当前签到时间为%s---%s') % (StartTime, StopTime))
                                    return
                                else:
                                    print('请输入正确数字！')
                        elif Choose.lower() == 'n' or Choose == '否':
                            StartTime = datetime.datetime.now()
                            StopTime = (StartTime + datetime.timedelta(minutes=10)).strftime('%Y-%m-%d %H:%M:%S')
                            StartTime = StartTime.strftime('%Y-%m-%d %H:%M:%S')
                            with open('Zhy.txt', encoding='utf-8', mode='w') as f:
                                f.write(("%s,%s\n") % (StartTime, StopTime))
                            print(('当前签到时间为%s---%s') % (StartTime, StopTime))
                            return
                elif Choose == '否' or Choose.lower() == 'n':
                    print('那就只能退出了')
                    exit()
                else:
                    print('输入有误！！')
    elif Name ==  'Nym':
        if len(NymTime) > 0:
            while True:
                print('是否提前结束？(y/n)')
                Choose = input('>>>').strip()
                if Choose == '是' or Choose.lower() == 'y':
                    Time = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                    NymTime.pop()
                    NymTime.append(Time)
                    with open('Nym.txt',encoding='utf-8',mode='w') as f:
                        f.write(("%s,%s\n")%(NymTime[0],NymTime[1]))
                        f.write(NymNum)
                        f.write('\n')
                        for i in NymName:
                            f.write(i)
                        f.write('\n')
                        for i in NymStTime:
                            f.write(i)
                    return
                elif Choose == '否' or Choose.lower() == 'n':
                    return
                else:
                    print('输入有误！')
        else:
            while True:
                print('您还未开始签到是否开始？(y/n)')
                Choose = input('>>>').strip()
                if Choose == '是' or Choose.lower() == 'y':
                    while True:
                        print('是否输入结束时间？（y/n）')
                        Choose = input('>>>').strip()
                        if  Choose.lower() == 'y'  or Choose == '是':
                            while True:
                                print('请输入多久分钟后结束？')
                                Choose = input('>>>').strip()
                                if Choose.isdigit():
                                    Choose = int(Choose)
                                    StartTime = datetime.datetime.now()
                                    StopTime = (StartTime + datetime.timedelta(minutes=Choose)).strftime('%Y-%m-%d %H:%M:%S')
                                    StartTime = StartTime.strftime('%Y-%m-%d %H:%M:%S')
                                    with open('Nym.txt', encoding='utf-8', mode='w') as f:
                                        f.write(("%s,%s\n") % (StartTime, StopTime))
                                    print(('当前签到时间为%s---%s') % (StartTime, StopTime))
                                    return
                                else:
                                    print('请输入正确数字！')
                        elif Choose.lower() == 'n' or Choose == '否':
                            StartTime = datetime.datetime.now()
                            StopTime = (StartTime + datetime.timedelta(minutes=10)).strftime('%Y-%m-%d %H:%M:%S')
                            StartTime = StartTime.strftime('%Y-%m-%d %H:%M:%S')
                            with open('Nym.txt', encoding='utf-8', mode='w') as f:
                                f.write(("%s,%s\n") % (StartTime, StopTime))
                            print(('当前签到时间为%s---%s') % (StartTime, StopTime))
                            return
                elif Choose == '否' or Choose.lower() == 'n':
                    print('那就只能退出了')
                    exit()
                else:
                    print('输入有误！！')
    elif Name ==  'Fjy':
        if len(FjyTime) >0:
            while True:
                print('是否提前结束？(y/n)')
                Choose = input('>>>').strip()
                if Choose == '是' or Choose.lower() == 'y':
                    Time = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                    FjyTime.pop()
                    FjyTime.append(Time)
                    with open('Fjy.txt',encoding='utf-8',mode='w') as f:
                        f.write(("%s,%s\n")%(FjyTime[0],FjyTime[1]))
                        f.write(FjyNum)
                        f.write('\n')
                        for i in FjyName:
                            f.write(i)
                        f.write('\n')
                        for i in FjyStTime:
                            f.write(i)
                    return
                elif Choose == '否' or Choose.lower() == 'n':
                    return
                else:
                    print('输入有误！')
        else:
            while True:
                print('您还未开始签到是否开始？(y/n)')
                Choose = input('>>>').strip()
                if Choose == '是' or Choose.lower() == 'y':
                    while True:
                        print('是否输入结束时间？（y/n）')
                        Choose = input('>>>').strip()
                        if  Choose.lower() == 'y'  or Choose == '是':
                            while True:
                                print('请输入多久分钟后结束？')
                                Choose = input('>>>').strip()
                                if Choose.isdigit():
                                    Choose = int(Choose)
                                    StartTime = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                                    StopTime = (StartTime + datetime.timedelta(minutes=Choose)).strftime('%Y-%m-%d %H:%M:%S')
                                    StartTime.strftime('%Y-%m-%d %H:%M:%S')
                                    with open('Fjy.txt', encoding='utf-8', mode='w') as f:
                                        f.write(("%s,%s\n") % (StartTime, StopTime))
                                    print(('当前签到时间为%s---%s') % (StartTime, StopTime))
                                    return
                                else:
                                    print('请输入正确数字！')
                        elif Choose.lower() == 'n' or Choose == '否':
                            Choose = int(Choose)
                            StartTime = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                            StopTime = (StartTime + datetime.timedelta(minutes=10)).strftime('%Y-%m-%d %H:%M:%S')
                            StartTime.strftime('%Y-%m-%d %H:%M:%S')
                            with open('Fjy.txt', encoding='utf-8', mode='w') as f:
                                f.write(("%s,%s\n") % (StartTime, StopTime))
                            print(('当前签到时间为%s---%s') % (StartTime, StopTime))
                            return
                elif Choose == '否' or Choose.lower() == 'n':
                    print('那就只能退出了')
                else:
                    print('输入有误！！')
def PrintSinPeople(Name):
    num = 1
    form = PrettyTable(['编号', '签到人', '签到时间'])  # 定义表头
    if Name == 'Qxz':
        for i in QxzPeople:  # 定义表格内容
            form.add_row([num,i,QxzStTime[QxzPeople.index(i)]])
            form.align = 'l'  # 所有内容左对齐
            num += 1
        print(form)
        print(('应签到50人，签到%s人，剩%s人未签')%(QxzNum,50 - int(QxzNum)))
        return
    elif Name == 'Dzg':
        for i in DzgPeople:
            form.add_row([num,i,DzgStTime[DzgPeople.index(i)]])
            form.align = 'l'
            num +=1
        print(form)
        print(('应签到50人，签到%s人，剩%s人未签') % (DzgNum, 50 - int(DzgNum)))
        return
    elif Name == 'Nym':
        for i in NymPeople:
            form.add_row([num,i,NymStTime[NymPeople.index(i)]])
            form.align = 'l'
            num += 1
        print(form)
        print(('应签到50人，签到%s人，剩%s人未签') % (NymNum, 50 - int(NymNum)))
        return
    elif Name == 'Zhy':
        for i in ZhyPeople:
            form.add_row([num,i,ZhyStTime[ZhyPeople.index(i)]])
            form.align = 'l'
            num += 1
        print(form)
        print(('应签到50人，签到%s人，剩%s人未签') % (ZhyNum, 50 - int(ZhyNum)))
        return
    elif Name == 'Fjy':
        for i in FjyPeople:
            form.add_row([num,i,FjyStTime[FjyPeople.index(i)]])
            form.align = 'l'
            num += 1
        print(form)
        print(('应签到50人，签到%s人，剩%s人未签') % (FjyNum, 50 - int(FjyNum)))
        return
def Teacher(UserName):
    print('\n')
    print(('欢迎%s老师' % UserName).center(50, '*'))
    Name = Judge(UserName)
    while True:
        Result = TeacherChoose()
        if Result == 1:
            Sign(Name)
        elif Result == 2 :
            ChangePassWd(UserName)
        elif Result == 3:
            os.system('read_data.py')
            PrintSinPeople(Name)
            
        else:
            exit()
