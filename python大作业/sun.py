#!/usr/bin/env python 
#-*- coding:utf-8 -*-
from prettytable import PrettyTable
from read_data import *
from login import *
TeacherName = []  # 老师姓名缩写
TeacherSign = [] #当前签到老师
def ReadTeach():  # 判断那些老师现在在签到
    if QxzTime != []:  # 如果老师签到时间不为空
        TeacherName.append('qxz')  # 添加老师缩写状态
        TeacherSign.append('祁相志')  # 老师签到状态添加该老师
    elif ZhyTime != []:
        TeacherName.append('zhy')
        TeacherSign.append('赵侯宇')
    elif NymTime != []:
        TeacherName.append('nym')
        TeacherSign.append('牛彦敏')
    elif DzgTime != []:
        TeacherName.append('dzg')
        TeacherSign.append('戴政国')
    elif FjyTime != []:
        TeacherName.append('fjy')
        TeacherSign.append('范金艳')
    elif QxzTime == [] and ZhyTime == [] and NymTime == [] and DzgTime == [] and FjyTime == []:
        return '当前没有老师签到'
    return TeacherName, TeacherSign
def SunChoose():
    print("\n")
    print('\t1.查询\t2.修改密码\t3.退出')
    while True:
        Choose = input('>>>').strip()
        if  Choose == '1' or  Choose == '查询':
            return 1
        elif Choose == '2' or Choose == '修改密码':
            return 2
        elif   Choose == '3' or Choose == '退出' or Choose.lower() == 'b':
            return 3
        else:
            print('输入有误！')
def Query():
    print('当前以下老师正在签到：')
    for i in TeacherSign:
        print('\t%d.%s' % (TeacherSign.index(i), i), end='')  # 打印正在签到老师的名单
    while True:
        num = 1
        print('\n输入编号或者姓名缩写以查看签到情况')
        UserChoose = input('>>>').strip()
        if UserChoose.isdigit():  # 是输入的编号时
            UserChoose = int(UserChoose)
            if UserChoose <= len(TeacherSign) - 1:  # 判断编号是不是存在
                if TeacherSign[UserChoose] == '祁相志':
                    form = PrettyTable(['编号', '签到人', '签到时间'])  # 定义表头
                    for i in QxzPeople:  # 定义表格内容
                        form.add_row([num, i, QxzStTime[QxzPeople.index(i)]])
                        form.align = 'l'  # 所有内容左对齐
                        num += 1
                    print(form)
                    print(('应签到50人，签到%s人，剩%s人未签') % (QxzNum, 50 - int(QxzNum)))
                    return
                elif TeacherSign[UserChoose] == '戴政国':
                    form = PrettyTable(['编号', '签到人', '签到时间'])  # 定义表头
                    for i in DzgPeople:
                        form.add_row([num, i, DzgStTime[DzgPeople.index(i)]])
                        form.align = 'l'
                        num += 1
                    print(form)
                    print(('应签到50人，签到%s人，剩%s人未签') % (DzgNum, 50 - int(DzgNum)))
                    return
                elif TeacherSign[UserChoose] == '赵侯宇':
                    form = PrettyTable(['编号', '签到人', '签到时间'])  # 定义表头
                    for i in ZhyPeople:
                        form.add_row([num, i, ZhyStTime[ZhyPeople.index(i)]])
                        form.align = 'l'
                        num += 1
                    print(form)
                    print(('应签到50人，签到%s人，剩%s人未签') % (ZhyNum, 50 - int(ZhyNum)))
                    return
                elif TeacherSign[UserChoose] == '牛彦敏':
                    form = PrettyTable(['编号', '签到人', '签到时间'])  # 定义表头
                    for i in NymPeople:
                        form.add_row([num, i, NymStTime[NymPeople.index(i)]])
                        form.align = 'l'
                        num += 1
                    print(form)
                    print(('应签到50人，签到%s人，剩%s人未签') % (NymNum, 50 - int(NymNum)))
                    return
                elif TeacherSign[UserChoose] == '范金艳':
                    form = PrettyTable(['编号', '签到人', '签到时间'])  # 定义表头
                    for i in FjyPeople:
                        form.add_row([num, i, FjyStTime[FjyPeople.index(i)]])
                        form.align = 'l'
                        num += 1
                    print(form)
                    print(('应签到50人，签到%s人，剩%s人未签') % (FjyNum, 50 - int(FjyNum)))
                    return
                else:
                    print('请输入正确的编号')
            else:
                print('请输入正确编号！')
        elif UserChoose.lower() in TeacherName:  # 输入姓名缩写时
            if UserChoose.lower() == 'qxz':
                form = PrettyTable(['编号', '签到人', '签到时间'])  # 定义表头
                for i in QxzPeople:  # 定义表格内容
                    form.add_row([num, i, QxzStTime[QxzPeople.index(i)]])
                    form.align = 'l'  # 所有内容左对齐
                    num += 1
                print(form)
                print(('应签到50人，签到%s人，剩%s人未签') % (QxzNum, 50 - int(QxzNum)))
                return
            elif UserChoose.lower() == 'dzg':
                form = PrettyTable(['编号', '签到人', '签到时间'])  # 定义表头
                for i in DzgPeople:
                    form.add_row([num, i, DzgStTime[DzgPeople.index(i)]])
                    form.align = 'l'
                    num += 1
                print(form)
                print(('应签到50人，签到%s人，剩%s人未签') % (DzgNum, 50 - int(DzgNum)))
                return
            elif UserChoose.lower() == 'zhy':
                form = PrettyTable(['编号', '签到人', '签到时间'])  # 定义表头
                for i in ZhyPeople:
                    form.add_row([num, i, ZhyStTime[ZhyPeople.index(i)]])
                    form.align = 'l'
                    num += 1
                print(form)
                print(('应签到50人，签到%s人，剩%s人未签') % (ZhyNum, 50 - int(ZhyNum)))
                return
            elif UserChoose.lower() == 'nym':
                form = PrettyTable(['编号', '签到人', '签到时间'])  # 定义表头
                for i in NymPeople:
                    form.add_row([num, i, NymStTime[NymPeople.index(i)]])
                    form.align = 'l'
                    num += 1
                print(form)
                print(('应签到50人，签到%s人，剩%s人未签') % (NymNum, 50 - int(NymNum)))
                return
            elif UserChoose.lower() == 'fjy':
                form = PrettyTable(['编号', '签到人', '签到时间'])  # 定义表头
                for i in FjyPeople:
                    form.add_row([num, i, FjyStTime[FjyPeople.index(i)]])
                    form.align = 'l'
                    num += 1
                print(form)
                print(('应签到50人，签到%s人，剩%s人未签') % (FjyNum, 50 - int(FjyNum)))
                return
        else:
            print('输入有误！')
def ChangeSunPassWd(): #修改密码3
    while True:
        PassWd = PasswdNoSee('请输入当前密码：')
        if PassWd == SunPassWd:
            while True:
                print('\n')
                PassWd1 = PasswdNoSee('请输入新密码：')
                print('\n')
                PassWd2 = PasswdNoSee('请再次输入密码：')
                if PassWd1 == PassWd2:
                    print('\n您已成功修改密码！')
                    TeacherPassWd[TeacherName.index(UserName)] = PassWd2
                    with open('Sun.txt',encoding='utf-8',mode='w') as f:
                        f.write(('%s,%s\n')%(Sun,PassWd1))
                    return
                else:
                    print('\n')
                    print('两次密码不相等')
        else:
            print('\n密码不正确！')
def Sun():
    print('\n')
    print(("欢迎辅导员孙柳艳").center(50, '*'))
    Result = ReadTeach()
    if len(Result) == 2:
        while True:
            Result = SunChoose()
            print(Result)
            if Result == 1:
                Query()
            elif Result == 2:
                ChangeSunPassWd()
            elif Result == 3:
                exit()
    else:
        while True:
            Result = SunChoose()
            print(Result)
            if Result == 1:
                print("当前没有老师签到")
            elif Result == 2:
                ChangeSunPassWd()
            elif Result == 3:
                exit()
            
    
