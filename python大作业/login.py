#!/usr/bin/env python 
#-*- coding:utf-8 -*-
from read_data import *

def PasswdNoSee(a):#密码不可见
    import msvcrt, os
    print(a, end='', flush=True)
    li = []
    while 1:
        ch = msvcrt.getch()
        #回车
        if ch == b'\r':
            return b''.join(li).decode() #把list转换为字符串返回
            break
        #退格
        elif ch == b'\x08':
            if li:
                li.pop()
                msvcrt.putch(b'\b')
                msvcrt.putch(b' ')
                msvcrt.putch(b'\b')
        #Esc
        elif ch == b'\x1b':
            break
        else:
            li.append(ch)
            msvcrt.putch(b'*')
    return b''.join(li).decode()##

def Login():
    while True:
        UserName = input('请输入用户名：').strip()
        num = 0 #定义错误次数
        while True:
            if num<4 :
                if UserName in TeacherName:#老师
                    PassWd = PasswdNoSee('请输入密码：')
                    if PassWd == TeacherPassWd[TeacherName.index(UserName)]:
                        return 'Teacher',UserName
                    else:
                        print('\n密码错误！')
                        num  += 1
                elif UserName in StudentNum: #学号
                    UserName = StudentName[StudentNum.index(UserName)]
                    PassWd = PasswdNoSee('请输入密码：')
                    if PassWd == StudentPassWd[StudentName.index(UserName)]:
                        return 'Student', UserName
                    else:
                        print('\n密码错误！')
                        num += 1
                elif UserName in StudentName :#学生
                    PassWd = PasswdNoSee('请输入密码：')
                    if PassWd == StudentPassWd[StudentName.index(UserName)] :
                        return 'Student',UserName
                    else:
                        print('\n密码错误！')
                        num += 1
                elif UserName == '孙柳燕':#孙柳燕
                    PassWd = PasswdNoSee('请输入密码：')
                    if PassWd == SunPassWd:
                        return 'Sun'
                    else:
                        print('\n密码错误！')
                        num += 1
                else:
                    print('该用户不存在啊')
                    break
            else:
                exit()