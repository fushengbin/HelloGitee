#!/usr/bin/env python
#-*- coding:utf-8 -*-
while 1:
    del_space = []
    user_input = input("请输入5位数字：")
    for i in user_input:
        if i ==" ":
            pass
        else:
            del_space.append(i)
    user_input = ''.join(del_space)
    if user_input.isdigit() :
        if len(user_input) == 5:
            print("万位：%d"%(int(user_input)//10000))
            print("千位：%d"%(int(user_input)%10000//1000))
            print("百位：%d" % (int(user_input) % 1000 //100))
            print("十位：%d" % (int(user_input) % 100//10))
            print("个位：%d" % (int(user_input) % 10))
            print("可以按n退出")
        else:
            print("5位数字！！！")
    elif user_input.lower() == 'n':
        break
    else:
        print("请输入数字！！！！")
        