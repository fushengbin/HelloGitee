#!/usr/bin/env python 
#-*- coding:utf-8 -*-
q=input("请输入一个五位数:")
if(q.isalnum()==True and len(q)==5):
    a=int(q)
    b=a//10000
    print("万位数为{}".format(b))
    c=a%10000//1000
    print("千位数为{}".format(c))
    d=a%1000//100
    print("百位数为{}".format(d))
    e=a%100//10
    print("十位数为{}".format(e))
    f=a%10
    print("个位数为{}".format(f))
else:
    print("error")