#!/usr/bin/env python 
#-*- coding:utf-8 -*-
import sys
def password_no_see():
    import msvcrt, os
    print('密码>>', end='', flush=True)
    li = []
    while 1:
        ch = msvcrt.getch()
        #回车
        if ch == b'\r':
            return b''.join(li).decode() #把list转换为字符串返回
            break
        #退格
        elif ch == b'\x08':
            if li:
                li.pop()
                msvcrt.putch(b'\b')
                msvcrt.putch(b' ')
                msvcrt.putch(b'\b')
        #Esc
        elif ch == b'\x1b':
            break
        else:
            li.append(ch)
            msvcrt.putch(b'*')
    return b''.join(li).decode()
user = {'zhang':'zhang','qi':'qi'}
print('-----------login-------------')
num = 0
while 1 :
    num += 1
    user_input = input('用户名>>')
    user_passwd_input = password_no_see()
    if num < 3 :
        if user_input in user.keys() :
            if user_passwd_input == user[user_input]:
                print(('\n欢迎%s登陆')%(user_input))
                break
            else:
                print(("\n用户%s密码错误")%(user_input))
        else:
            print(("\n用户%s不存在，请核实后再登陆")%(user_input))
    else:
        print('\n错误次数过多请稍后再试')
        break
