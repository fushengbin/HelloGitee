#!/usr/bin/env python 
#-*- coding:utf-8 -*-
# 方法1，自己做的(不能出现重复数据，否则就有问题)
# def set_up_a_new_list_by_a_old_list(old_list):
#     new_list = []
#     for i in old_list:
#         if old_list.index(i) % 2 == 0:
#             new_list.append(i)
#         else:
#             pass
#     print(new_list)
# set_up_a_new_list_by_a_old_list(['daddsa','dasdas','weqrqwe','ewqrsd'])
# 方法2（不使用循环）：
# def get_date_list(old_list):
#     v = old_list[::2]
#     return v
# new_list = get_date_list([23131,3214324,432423,432,5,25432,432,423,432,4,23])
# print(new_list)
# 方法3：
def get_date_list(old_list):
    new_list = []
    for i in range(0,len(old_list)):
        if i % 2 == 0 :
            new_list.append(old_list[i])
        else:
            pass
    print(new_list)
new_list = get_date_list([23132,313,3213,1,4,3,543,6,43,56])
