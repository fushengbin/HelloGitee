# 制作趣味模板程序（编程题）需求：等待用户输入名字、地点、爱好，
# 根据用户的名字和爱好进行任意显示：如：敬爱可爱的xxx，最喜欢在xxx地方干xxx。
# who = input('''What's your name : ''' )
# where = input(' Where do you like to go : ')
# what = input(' What do you like to do best : ')
# print(('可爱的%s,喜欢在%s,干%s')%(who,where,what))
'''输入一年份，判断该年份是否是闰年并输出结果。（编程题）
注：凡符合下面两个条件之一的年份是闰年。
a. 能被4整除但不能被100整除；b.能被400整除；'''
# a方法:
'''year = int(input('请输入年份:'))
if year % 4 == 0 and year % 100 != 0:
    print('恭喜,你判断的是闰年')
else:
    print('soorry , 您输入的不是闰年.')'''
#b方法：
'''year = int(input('请输入您想判断的年份：'))
if year % 400 == 0 :
    print('恭喜，您判断的是闰年')
else:
    print('sorry , 您输入的不是闰年')'''
# 假设一年期定期利率为3.25%，计算一下需要过多少年，一万元的一年定期存款连本带息能翻番？
money = 10000
year = 0
while money <= 20000 :
    money = money + money * 0.0325
    year += 1
print(('过了%d年，您的1万元成了2万')%(year))
# 使用while循环，实现以下图形输出：
'''*
* *
* * *
* * * *
* * * * *
* * * *
* * *
* *
*
'''
'''
while 1:
    print('*' )
    print('**')
    print('***')
    print('****')
    print('*****')
    print('****')
    print('***')
    print('**')
    print('*')
    break
'''


