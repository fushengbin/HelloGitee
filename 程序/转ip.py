#!/usr/bin/env python 
#-*- coding:utf-8 -*-
# 将ip转化成8位的二进制，然后再将其每个拼接起来，再转化成新的十进制
ip = '102.213.41.4'
ip = ip.split('.')
ip_2 = []
for i in ip :
    ip_2.append(bin(int(i)))
ip_10 = []
for i in ip_2:
    if len(i)==10:
        i = list(i)
        ip_10.append(i[2:])
    if len(i) <10:
        i = list(i)
        while True:
            i.insert(2,'0')
            if len(i) == 10 :
                ip_10.append(i[2:])
                break
            else:
                continue
new_ip_2 = []
for i in ip_10:
    new_ip_2.append(' '.join(i))
new_ip_10_end =''.join(new_ip_2)
end_off = []
for i in new_ip_10_end:
    if i.isdigit():
        end_off.append(i)
    else:
        pass
end_off = ''.join(end_off)
print(int(end_off,2))
        