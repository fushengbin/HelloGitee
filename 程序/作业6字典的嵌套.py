dic = {
    ' 太白  ':{ 'name ' : '太白金星' , ' age ' : 25 , 'sex ' : ' 男 '},
    1 : [ ' 张三 ' ,'李四' , ' alex ' ],
}
# 1.给'太白'对应的字典里添加一个键值对,'weight' : 75
''' dic[' 太白  ']['weight'] = 75
print(dic)'''

# 2.将'太白'对应字典里面'age'的值改为18
'''dic[' 太白  ']['age'] = 18
print(dic)'''

# 3.将1对应的列表追加一个'WuSir'
'''dic[1].append('WuSir')
print(dic)'''
# 4.将1对应的alex大写
'''dic[1][2] = dic[1][2].upper()
print(dic)'''
# 5.给太白对应的字典新加一个键值对，'hobby_list' : [ '抽烟 ' , ' 喝酒 ' ,' 没法烫头 ']
'''dic[' 太白  ']['hobby_list'] = [ '抽烟 ' , ' 喝酒 ' ,' 没法烫头 ']
print(dic)'''