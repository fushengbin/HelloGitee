#!/usr/bin/env python
#-*- coding:utf-8 -*-
from prettytable import PrettyTable
import datetime
import os
ShopList = {} #商品菜单
UserShopList = [] #用户已买菜单
ShopName = [] #商品名字
ShopPrice = [] #商品价格
NumList = [] #编号
ByShopTime = [] #商品购买时间
UserMoney = 0 #用户金钱
ShopNum = [] #商品数量
AdminUser = [] #管理员用户
AdminPassWd = [] #管理员密码
def ReadUserInfo():
    global UserShopList
    global ByShopTime
    global UserMoney
    global UserName
    global SaveChoose
    if UserName in AdminUser:
        while True:
            print("请选择以测试身份还是普通")
            print('1.测试\t2.普通用户')
            UserChoose = input(">>>")
            if UserChoose == '1' or '测试':
                SaveChoose = UserChoose
                if os.path.exists('%s_date.txt' % (UserName)) == True:
                    UserInfo = open('%s_date.txt' % (UserName), mode='r', encoding='utf-8')
                    UserShopList = UserInfo.readline().strip().split(',')  # 读取已买商品名
                    UserShopList.pop()  # 删除列表最后一个空的
                    ByShopTime = UserInfo.readline().strip().split(',')  # 读取已买商品时间
                    ByShopTime.pop()  # 删除列表最后一个空的
                    UserMoney = int(UserInfo.readline())  # 读取用户余额
                    UserInfo.close()
                    shopping()
                else:
                    InputUserMoney()
                    shopping()
            elif UserChoose == '2' or UserChoose == '普通用户':
                SaveChoose = UserChoose
                UserInfo = open('%s_date.txt' % (UserName), mode='r', encoding='utf-8')
                PassWd = str(UserInfo.readline()).strip()  # 读取密码
                UserShopList = UserInfo.readline().strip().split(',')  # 读取已买商品名
                UserShopList.pop()  # 删除列表最后一个空的
                ByShopTime = UserInfo.readline().strip().split(',')  # 读取已买商品时间
                ByShopTime.pop()  # 删除列表最后一个空的
                UserMoney = int(UserInfo.readline())  # 读取用户余额
                UserInfo.close()
                shopping()
            else:
                print('输入有误')
def ReadAdmin():#读取管理员账户以及密码
    global AdminUser
    global AdminPassWd
    UserInfo = open('Admin.txt',mode='r',encoding='utf-8')
    for lines in UserInfo:
        AdminUser.append(lines[0:lines.index(':')])
        AdminPassWd.append(lines[(lines.index(':')+1):].strip())
    return AdminUser,AdminPassWd
def ReadShop():#读取商品列表
    global ShopNum
    global ShopList
    ReadShopList = open('ShopList.txt',mode='r',encoding='utf-8')
    for lines in ReadShopList:
        lines.strip()
        key = lines[0:lines.index(':')]
        value = lines[(lines.index(':')+1):lines.index(',')]
        ShopList[key] = int(value)
        ShopNum.append(int(lines[lines.index(',') + 1:].strip()))
def AdminOperation(): #管理员操作
    global ShopList
    global NumList
    global ShopName
    global ShopPrice
    global ShopNum
    print('欢迎管理员%s进入'%UserName)
    while True:
        print('1.用户操作\t2.修改商品\t3.退出（b）')
        AdminChoose = input('请输入您要进行的操作：').strip() #管理员选择操作
        if AdminChoose == '1' or AdminChoose =='用户管理' or AdminChoose == '用户操作':
            while True:
                All = PrintUserList()
                UserList = All[0]
                NumList = All[1]
                print('选择用户或者编号进行提权或者降权（可以退出或者返回上一层）：')
                AdminChoose = input('>>>').strip()
                if AdminChoose.isdigit():
                    AdminChoose = int(AdminChoose)
                    if AdminChoose in  NumList:
                        if UserName == 'admin': #超级管理员
                            while True:
                                if UserList[NumList.index(AdminChoose)] in AdminUser:
                                    print('您已选择管理员用户%s'%UserList[NumList.index(AdminChoose)])
                                else:
                                    print('您已选择用户%s'%UserList[NumList.index(AdminChoose)])
                                print('1.提权\t2.降权')
                                AdminChoose2 = input('>>>').strip()
                                if AdminChoose2 == '1' :
                                    AdminChoose2 = int(AdminChoose2)
                                    if UserList[NumList.index(AdminChoose)] in AdminUser:
                                        print('操作失败！！\n该用户已经是管理员了，无法提权了啊！')
                                        break
                                    else:
                                        AdminUser.append(UserList[NumList.index(AdminChoose)])
                                        with open('%s_date.txt'%UserList[NumList.index(AdminChoose)],mode='r',encoding='utf-8')as UserPasswd:
                                            PassWd = str(UserPasswd.readline()).strip()  # 读取密码
                                        AdminPassWd.append(PassWd)
                                        print('操作成功！')
                                        break
                                elif AdminChoose2 == '2':
                                    AdminChoose2 = int(AdminChoose2)
                                    if UserName == UserList[NumList.index(AdminChoose)]:
                                        print("您无法对自己进行操作啊！")
                                        break
                                    else:
                                        AdminUser.pop(NumList.index(AdminChoose))
                                        AdminPassWd.pop(NumList.index(AdminChoose))
                                        print('操作成功！')
                                        break
                                elif AdminChoose2 == '提权' :
                                    if UserList[NumList.index(AdminChoose)] == UserName:
                                        print('您无法对自己进行操作')
                                        break
                                    else:
                                        AdminUser.append(UserList[NumList.index(AdminChoose)])
                                        with open('%s_date.txt'%UserList[NumList.index(AdminChoose)],mode='r',encoding='utf-8')as UserPasswd:
                                            PassWd = str(UserPasswd.readline()).strip()  # 读取密码
                                        AdminPassWd.append(PassWd)
                                        print('操作成功！')
                                        break
                                elif AdminChoose2 == '降权':
                                    if UserList[NumList.index(AdminChoose)] == UserName:
                                        print('您无法对自己进行操作')
                                        break
                                    else:
                                        AdminUser.pop(UserList[NumList.index(AdminChoose)])
                                        with open('%s_date.txt'%UserList[NumList.index(AdminChoose)],mode='r',encoding='utf-8')as UserPasswd:
                                            PassWd = str(UserPasswd.readline()).strip()  # 读取密码
                                        AdminPassWd.pop(AdminPassWd.index(PassWd))
                                        print('操作成功！')
                                        break
                                elif AdminChoose2.lower() == 'b' or AdminChoose2 == '上一层':
                                    break
                                else:
                                    print('抱歉不知道您要做什么!')
                        elif UserList[NumList.index(AdminChoose)] in AdminUser: #其它管理员用户的操作
                            while True:
                                if UserList[NumList.index(AdminChoose)] in AdminUser:
                                    print('您已选择管理员用户%s'%UserList[NumList.index(AdminChoose)])
                                else:
                                    print('您已选择用户%s'%UserList[NumList.index(AdminChoose)])
                                print('1.提权\t2.降权')
                                AdminChoose2 = input('>>>').strip()
                                if AdminChoose2 == '1':
                                    AdminChoose2 = int(AdminChoose2)
                                    if UserList[NumList.index(AdminChoose)] in AdminUser:
                                        print('操作失败！！\n该用户已经是管理员了！')
                                        break
                                    else:
                                        AdminUser.append(UserList[NumList.index(AdminChoose)])
                                        with open('%s_date.txt' % UserList[NumList.index(AdminChoose)], mode='r',
                                                  encoding='utf-8')as UserPasswd:
                                            PassWd = str(UserPasswd.readline()).strip()  # 读取密码
                                        AdminPassWd.append(PassWd)
                                        print('操作成功！')
                                        break
                                elif AdminChoose2 == '提权':
                                    if UserList[NumList.index(AdminChoose)] in AdminUser:
                                        print('操作失败！！\n该用户已经是管理员了！')
                                        break
                                    else:
                                        AdminUser.append(UserList[NumList.index(AdminChoose)])
                                        with open('%s_date.txt' % UserList[NumList.index(AdminChoose)], mode='r',
                                                  encoding='utf-8')as UserPasswd:
                                            PassWd = str(UserPasswd.readline()).strip()  # 读取密码
                                        AdminPassWd.append(PassWd)
                                        print('操作成功！')
                                        break
                                elif AdminChoose2 == '2':
                                    AdminChoose2 = int(AdminChoose2)
                                    if UserList[NumList.index(AdminChoose)] not in AdminUser:
                                        print('操作失败！！\n该用户已经不是管理员了，无法降权了啊！')
                                        break
                                    elif UserList[NumList.index(AdminChoose)] in AdminUser:
                                        print('操作失败！！\n该用户也是管理员，您无权对其进行操作！')
                                        break
                                    else:
                                        AdminUser.pop(UserList[NumList.index(AdminChoose)])
                                        with open('%s_date.txt' % UserList[NumList.index(AdminChoose)], mode='r',
                                                  encoding='utf-8')as UserPasswd:
                                            PassWd = str(UserPasswd.readline()).strip()  # 读取密码
                                        AdminPassWd.pop(AdminPassWd.index(PassWd))
                                        print('操作成功！')
                                        break
                                elif AdminChoose2 == '降权':
                                    if UserList[NumList.index(AdminChoose)] not in AdminUser:
                                        print('操作失败！！\n该用户已经不是管理员了，无法降权了啊！')
                                        break
                                    elif UserList[NumList.index(AdminChoose)] in AdminUser:
                                        print('操作失败！！\n该用户也是管理员，您无权对其进行操作！')
                                        break
                                    else:
                                        AdminUser.pop(UserList[NumList.index(AdminChoose)])
                                        with open('%s_date.txt' % UserList[NumList.index(AdminChoose)], mode='r',
                                                  encoding='utf-8')as UserPasswd:
                                            PassWd = str(UserPasswd.readline()).strip()  # 读取密码
                                        AdminPassWd.pop(AdminPassWd.index(PassWd))
                                        print('操作成功！')
                                        break
                                elif AdminChoose2.lower() == 'b' or AdminChoose2 == '上一步':
                                    break
                                else:
                                    print("抱歉不知道您要干什么")
                elif AdminChoose in AdminUser:
                    if UserName == 'admin': #超级管理员
                        while True:
                            if UserList[UserList.index(AdminChoose)] in AdminUser:
                                print('您已选择管理员用户%s'%UserList[UserList.index(AdminChoose)])
                            else:
                                print('您已选择用户%s'%UserList[UserList.index(AdminChoose)])
                            print('1.提权\t2.降权')
                            AdminChoose2 = input('>>>').strip()
                            if AdminChoose2.isdigit():
                                AdminChoose2 = int(AdminChoose2)
                                if AdminChoose2 == 1 :
                                    if UserList[UserList.index(AdminChoose)] in AdminUser:
                                        print('操作失败！！\n该用户已经是管理员了，无法提权了啊！')
                                        break
                                    else:
                                        AdminUser.append(UserList[UserList.index(AdminChoose)])
                                        with open('%s_date.txt'%UserList[UserList.index(AdminChoose)],mode='r',encoding='utf-8')as UserPasswd:
                                            PassWd = str(UserPasswd.readline()).strip()  # 读取密码
                                        AdminPassWd.append(PassWd)
                                        print('操作成功！')
                                        break
                                elif AdminChoose2 == 2:
                                    if UserList[UserList.index(AdminChoose)] not in AdminUser:
                                        print('操作失败！！\n该用户已经不是管理员了，无法降权了啊！')
                                        break
                                    else:
                                        AdminUser.pop(UserList[UserList.index(AdminChoose)])
                                        AdminPassWd.pop(UserList[UserList.index(AdminChoose)])
                                        print('操作成功！')
                                        break
                            elif AdminChoose2 == '提权' :
                                if UserList[UserList.index(AdminChoose)] in AdminUser:
                                    print('操作失败！！\n该用户已经是管理员了，无法提权了啊！')
                                    break
                                else:
                                    AdminUser.append(UserList[UserList.index(AdminChoose)])
                                    with open('%s_date.txt'%UserList[UserList.index(AdminChoose)],mode='r',encoding='utf-8')as UserPasswd:
                                        PassWd = str(UserPasswd.readline()).strip()  # 读取密码
                                    AdminPassWd.append(PassWd)
                                    print('操作成功！')
                                    break
                            elif AdminChoose2 == '降权':
                                if UserList[UserList.index(AdminChoose)] == UserName:
                                    print('您无法对自己进行操作！')
                                    break
                                if UserList[UserList.index(AdminChoose)]  not in AdminUser:
                                    print('操作失败！！\n该用户已经不是管理员了，无法降权了啊！')
                                    break
                                else:
                                    AdminUser.pop(UserList[UserList.index(AdminChoose)])
                                    AdminPassWd.pop(UserList[UserList.index(AdminChoose)])
                                    print('操作成功！')
                                    break
                            else:
                                print("抱歉不知道您要干什么")
                    elif UserList[NumList.index(AdminChoose)] in AdminUser: #其它管理员用户的操作
                        while True:
                            if UserList[NumList.index(AdminChoose)] in AdminUser:
                                print('您已选择管理员用户%s'%UserList[NumList.index(AdminChoose)])
                                break
                            else:
                                print('您已选择用户%s'%UserList[NumList.index(AdminChoose)])
                            print('1.提权\t2.降权')
                            AdminChoose2 = input('>>>').strip()
                            if int(AdminChoose2) == 1:
                                if UserList[NumList.index(AdminChoose)] in AdminUser:
                                    print('操作失败！！\n该用户已经是管理员了！')
                                else:
                                    AdminUser.append(UserList[NumList.index(AdminChoose)])
                                    with open('%s_date.txt' % UserList[NumList.index(AdminChoose)], mode='r',
                                                  encoding='utf-8')as UserPasswd:
                                        PassWd = str(UserPasswd.readline()).strip()  # 读取密码
                                    AdminPassWd.append(PassWd)
                                    print('操作成功！')
                                    break
                            elif AdminChoose2 == '提权':
                                if UserList[NumList.index(AdminChoose)] in AdminUser:
                                    print('操作失败！！\n该用户已经是管理员了！')
                                    break
                                else:
                                    AdminUser.append(UserList[NumList.index(AdminChoose)])
                                    with open('%s_date.txt' % UserList[NumList.index(AdminChoose)], mode='r',
                                                  encoding='utf-8')as UserPasswd:
                                        PassWd = str(UserPasswd.readline()).strip()  # 读取密码
                                    AdminPassWd.append(PassWd)
                                    print('操作成功！')
                                    break
                            elif int(AdminChoose2) == 2:
                                if UserList[NumList.index(AdminChoose)] not in AdminUser:
                                    print('操作失败！！\n该用户已经不是管理员了，无法降权了啊！')
                                    break
                                elif UserList[NumList.index(AdminChoose)] in AdminUser:
                                    print('操作失败！！\n该用户也是管理员，您无权对其进行操作！')
                                    break
                                else:
                                    AdminUser.pop(UserList[NumList.index(AdminChoose)])
                                    with open('%s_date.txt' % UserList[NumList.index(AdminChoose)], mode='r',
                                                  encoding='utf-8')as UserPasswd:
                                        PassWd = str(UserPasswd.readline()).strip()  # 读取密码
                                    AdminPassWd.pop(AdminPassWd.index(PassWd))
                                    print('操作成功！')
                                    break
                            elif AdminChoose2 == '降权':
                                if UserList[NumList.index(AdminChoose)] not in AdminUser:
                                    print('操作失败！！\n该用户已经不是管理员了，无法降权了啊！')
                                    break
                                elif UserList[NumList.index(AdminChoose)] in AdminUser:
                                    print('操作失败！！\n该用户也是管理员，您无权对其进行操作！')
                                    break2
                                else:
                                    AdminUser.pop(UserList[NumList.index(int(AdminChoose))])
                                    with open('%s_date.txt' % UserList[NumList.index(int(AdminChoose))], mode='r',
                                                  encoding='utf-8')as UserPasswd:
                                            PassWd = str(UserPasswd.readline()).strip()  # 读取密码
                                    AdminPassWd.pop(AdminPassWd.index(PassWd))
                                    print('操作成功！')
                                    break
                            else:
                                print("抱歉不知道您要干什么")
                elif AdminChoose.lower() == 'b' or AdminChoose == '上一步' :
                    break
                elif AdminChoose == '退出':
                    exit()
                else:
                    print("输入有误！")
        elif AdminChoose == '2' or AdminChoose == '修改商品':
            while True:
                PrintShop()
                print("可以根据序号或者商品名选择修改商品名字，价格或者数量")
                print('输入增加或者删除用于增加或者删除商品')
                print('可以退出或者返回上一层')
                AdminChoose = input(">>>").strip()
                if AdminChoose.isdigit():
                    AdminChoose = int(AdminChoose)
                    if AdminChoose in NumList:
                        while True:
                            print("您已经选择商品%s"%ShopName[NumList.index(AdminChoose)])
                            print("请选择您接下来的操作：\n1.修改名字\n2.修改价格\n3.修改余量")
                            while True:
                                AdminChoose2 = input('>>>').strip()
                                if AdminChoose2 == '修改名字' or AdminChoose2 == '1':
                                    while True:
                                        print("您想将%s修改成什么？"%ShopName[NumList.index(AdminChoose)])
                                        AdminChoose3 = input('>>>').strip()
                                        print(('是否将%s修改成%s')%(ShopName[NumList.index(AdminChoose)],AdminChoose3))
                                        while True:
                                            AdminChoose4 = input(">>>").strip()
                                            if AdminChoose4.lower() == 'y' or AdminChoose4 == '是':
                                                del ShopList[ShopName[NumList.index(AdminChoose)]]
                                                ShopList[AdminChoose3] = ShopPrice[NumList.index(AdminChoose)]
                                                ShopName[NumList.index(AdminChoose)] = AdminChoose3
                                                print("修改成功！")
                                                break
                                            elif AdminChoose4.lower() == 'n' or AdminChoose4 == '否':
                                                break
                                            else:
                                                print("输入有误！")
                                        break
                                elif AdminChoose2 == '修改价格' or AdminChoose2 == '2':
                                    while True:
                                        print(("商品%s价格%s") % (ShopName[NumList.index(AdminChoose)],ShopPrice[NumList.index(AdminChoose)]))
                                        print("您想修改成多少钱？")
                                        AdminChoose3 = input('>>>').strip()
                                        if AdminChoose3.isdigit():
                                            while True:
                                                print("是否修改为%d元"%int(AdminChoose3))
                                                AdminChoose4 = input(">>>").strip()
                                                if AdminChoose4.lower() == 'y' or AdminChoose4 == '是':
                                                    print('操作成功')
                                                    ShopList[ShopName[NumList.index(AdminChoose)]] = int(AdminChoose3)
                                                    ShopPrice[NumList.index(AdminChoose)] = int(AdminChoose3)
                                                    break
                                                elif AdminChoose4.lower() == 'n' or AdminChoose4 == '否':
                                                    break
                                                else:
                                                    print("输入有误！")
                                        else:
                                            print("请输入数字")
                                            continue
                                        break
                                elif AdminChoose2 == '修改余量' or AdminChoose2 == '3':
                                    while True:
                                        print(("商品%s余量%d")%(ShopName[NumList.index(AdminChoose)],ShopNum[NumList.index(AdminChoose)]))
                                        print("您想修改成多少？")
                                        AdminChoose3 = input('>>>').strip()
                                        if AdminChoose3.isdigit():
                                            while True:
                                                print('是否修改为%d?'%int(AdminChoose3))
                                                AdminChoose4 = input(">>>").strip()
                                                if AdminChoose4.lower() == 'y' or AdminChoose4 == '是':
                                                    ShopNum[NumList.index(AdminChoose)] = int(AdminChoose3)
                                                    print("操作成功！")
                                                    break
                                        else:
                                            print("请输入数字！")
                                            continue
                                        break
                                else:
                                    print("输入有误！")
                                    continue
                                break
                            break
                    else:
                        print('\n没有您输入的序号')
                elif AdminChoose in ShopName:
                    while True:
                        print("您已经选择商品%s" % AdminChoose)
                        print("请选择您接下来的操作：\n1.修改名字\n2.修改价格\n3.修改余量")
                        while True:
                            AdminChoose2 = input('>>>').strip()
                            if AdminChoose2 == '修改名字' or AdminChoose2 == '1':
                                while True:
                                    print("您想将%s修改成什么？")
                                    AdminChoose3 = input('>>>').strip()
                                    print(('是否将%s修改成%s')%(AdminChoose,AdminChoose3))
                                    AdminChoose4 = input(">>>").strip()
                                    if AdminChoose4.lower() == 'y' or AdminChoose4 == '是':
                                        del ShopList[ShopName[NumList.index(AdminChoose)]]
                                        ShopList[AdminChoose3] = ShopPrice[NumList.index(AdminChoose)]
                                        ShopName[ShopName.index(AdminChoose)] = AdminChoose3
                                        print("修改成功！")
                                        PrintShop()
                                        break
                                    else:
                                        print("输入有误！")
                            elif AdminChoose2 == '修改价格' or AdminChoose2 == '2':
                                while True:
                                    print(("商品%s价格%d") % (AdminChoose,ShopPrice[ShopName.index(AdminChoose)]))
                                    print("您想修改成多少钱？")
                                    AdminChoose3 = input('>>>').strip()
                                    if AdminChoose3.isdigit():
                                        while True:
                                            print("是否修改为%d元"%int(AdminChoose3))
                                            AdminChoose4 = input(">>>").strip()
                                            if AdminChoose4.lower() == 'y' or AdminChoose4 == '是':
                                                print('操作成功')
                                                ShopList[ShopName[NumList.index(AdminChoose)]] = int(AdminChoose3)
                                                ShopPrice[NumList.index(AdminChoose)] = int(AdminChoose3)
                                                break
                                            else:
                                                print("输入有误！")
                                    else:
                                        print("请输入数字")
                                        continue
                                    break
                            elif AdminChoose2 == '修改余量' or AdminChoose2 == '3':
                                while True:
                                    print(("商品%s余量%d")%(AdminChoose,ShopNum[ShopName.index(AdminChoose)]))
                                    print("您想修改成多少？")
                                    AdminChoose3 = input('>>>').strip()
                                    if AdminChoose3.isdigit():
                                        while True:
                                            print('是否修改为%d?'%int(AdminChoose3))
                                            AdminChoose4 = input(">>>").strip()
                                            if AdminChoose4.lower() == 'y' or AdminChoose4 == '是':
                                                ShopNum[ShopName.index(AdminChoose)] = AdminChoose3
                                                print("操作成功！")
                                                PrintShop()
                                                break
                                    else:
                                        print("请输入数字！")
                                        continue
                                    break
                            else:
                                print('输入有误！')
                                continue
                            break
                elif AdminChoose == '增加':
                    while True:
                        print('请输入您想增加的商品：')
                        AdminChoose2 = input(">>>").strip()
                        while True:
                            print('请输入%s的价格：'%AdminChoose2)
                            AdminChoose3 = input(">>>").strip()
                            if AdminChoose3.isdigit():
                                while True:
                                    print('请输入%s的余量：' %AdminChoose2)
                                    AdminChoose4 = input(">>>").strip()
                                    if AdminChoose4.isdigit():
                                        ShopList[AdminChoose2] = int(AdminChoose3)
                                        ShopName.append(AdminChoose2)
                                        ShopPrice.append(int(AdminChoose3))
                                        ShopNum.append(AdminChoose4)
                                        print("操作成功！")
                                        break
                                    else:
                                        print('请输入数字！')
                            else:
                                print("请输入数字！")
                                continue
                            break
                        break
                elif AdminChoose == '删除':
                    while True:
                        print('请输入您要删除的商品名或者编号')
                        AdminChoose2 = input('>>>').strip()
                        if AdminChoose2.isdigit():
                            AdminChoose2 = int(AdminChoose2)
                            if AdminChoose2 in NumList:
                                while True:
                                    print('是否删除商品%s？'%ShopName[NumList.index(AdminChoose2)])
                                    AdminChoose3 = input('>>>').strip()
                                    if AdminChoose3.lower() == 'y' or AdminChoose3 == '是':
                                        ShopName.pop(NumList.index(AdminChoose2))
                                        ShopPrice.pop(NumList.index(AdminChoose2))
                                        ShopNum.pop(NumList.index(AdminChoose2))
                                        del ShopList[ShopName[NumList.index(AdminChoose2)]]
                                        print("操作成功！")
                                        PrintShop()
                                        break
                                    elif AdminChoose3.lower() == 'n' or AdminChoose3 == '否':
                                        print("好的")
                                        break
                                    else:
                                        print('输入有误')
                            else:
                                print('请输入正确的编号！')
                        elif AdminChoose2 in ShopName:
                            while True:
                                print('是否删除商品%s？' % AdminChoose2)
                                AdminChoose3 = input('>>>').strip()
                                if AdminChoose3.lower() == 'y' or AdminChoose3 == '是':
                                    ShopPrice.pop(ShopName.index(AdminChoose2))
                                    ShopNum.pop(ShopName.index(AdminChoose2))
                                    ShopName.pop(ShopName.index(AdminChoose2))
                                    del ShopList[AdminChoose2]
                                    print("操作成功！")
                                    PrintShop()
                                    break
                                elif AdminChoose3.lower() == 'n' or AdminChoose3 == '否':
                                    print("好的")
                                    break
                                else:
                                    print('输入有误')
                        else:
                            print("输入有误！")
                            continue
                        break
                elif AdminChoose.lower() == 'b' or AdminChoose == '上一步':
                    break
                elif AdminChoose == '退出':
                    exit()
                else:
                    print('输入有误！')
        elif AdminChoose == '3' or AdminChoose == '退出' or  AdminChoose.lower() == 'b':
            print('您已退出')
            SaveAdmin()
            SaveShop()
            exit()
        else:
            print("输入有误！")
def UserChoose():  #开始让用户选择是否直接进入购物车程序还是登陆
    ReadAdmin()
    while True:
        print("是登陆还是直接进入?")
        print('1.登录\t2.进入')
        UserChoose = input(">>>").strip()
        if UserChoose == '登陆' or UserChoose == '登录' or UserChoose == '1' :
            ReadShop()
            UserLogin()
        elif UserChoose == '进入'or UserChoose == '2':
            ReadShop()
            print("欢迎光临小张购物车".center(50, '*'))
            PrintShop()
            print("请输入您想购买的商品(编号和商品名都可)：")
            input('>>>')
            print('您还未登陆无权操作！')
            while True:
                print("是否登录？")
                print('1.登录\t2.退出')
                UserChoose = input(">>>").strip()
                if UserChoose == '是' or UserChoose.lower() == 'y' or UserChoose == '登录' or UserChoose == '1':
                    UserLogin()
                elif UserChoose == '否' or UserChoose.lower() == 'n' or UserChoose == '2':
                    print('再见！')
                    exit()
                else:
                    print('输入有误！')
        else:
            print("输入有误！")
def PasswdNoSee(a):#密码不可见
    import msvcrt, os
    print(a, end='', flush=True)
    li = []
    while 1:
        ch = msvcrt.getch()
        #回车
        if ch == b'\r':
            return b''.join(li).decode() #把list转换为字符串返回
            break
        #退格
        elif ch == b'\x08':
            if li:
                li.pop()
                msvcrt.putch(b'\b')
                msvcrt.putch(b' ')
                msvcrt.putch(b'\b')
        #Esc
        elif ch == b'\x1b':
            break
        else:
            li.append(ch)
            msvcrt.putch(b'*')
    return b''.join(li).decode()##
def UserLogin():#用户登录
    global UserName
    global PassWd
    global UserShopList
    global UserMoney
    global ByShopTime
    while True:
        UserName = input('请输入用户名：').strip()
        if UserName in AdminUser:
            num = 0
            while True:
                if num <= 3:
                    AdminPass = PasswdNoSee('请输入密码：')
                    print('\n')
                    if AdminPass == AdminPassWd[AdminUser.index(UserName)]:
                        PassWd = AdminPassWd
                        while True:
                            print("请选择以管理员登录或者普通用户？")
                            print("1.管理员\t2.用户")
                            UserChoose = input('>>>').strip()
                            if UserChoose == '1' or UserChoose == '管理员':
                                AdminOperation()
                            elif UserChoose == '2' or UserChoose == '用户':
                                ReadUserInfo()
                                PassWd = AdminPassWd[AdminUser.index(UserName)]
                                InputUserMoney()
                                shopping()
                            else:
                                print('输入有误')
                    else:
                        print('密码错误！')
                        num += 1
                else:
                    print('错误次数过多！')
                    exit()
        elif os.path.exists('%s_date.txt'%(UserName)) == True:#判断是否存在日志文件
            ReadUserInfo()
            num = 0
            while True :
                UserChoose = PasswdNoSee('请输入密码：')  #判断密码
                print('\n')
                if num < 3 :#错误次数小于3
                    if UserChoose == PassWd:
                        PrintUserShop()
                        print('您余额还有：%s' % UserMoney)
                        while True:
                            UserChoose = input('是否充值？（y/n）').strip()
                            if UserChoose.lower() == "y" or UserChoose == '是':
                                InputUserMoney()
                                shopping()
                            elif UserChoose.lower() == "n" or UserChoose == '否':
                                while True:
                                    print('退出还是继续购买？')
                                    print('1.继续\t2.退出')
                                    UserChoose = input('退出还是继续购买？').strip()
                                    if UserChoose.lower() == "b" or UserChoose.lower() == 'q' or UserChoose == '退出' or UserChoose =='2 ':
                                        exit()
                                    elif UserChoose == "购买" or UserChoose == '继续' or UserChoose.lower() == 'c' or UserChoose == '1':
                                        shopping()
                                        UserInfoSave(UserName)
                                    else:
                                        print('输入有误')
                            else:
                                print('输入有误')
                    else:
                        print('\n密码输入错误')
                        num += 1
                else:
                    print('\n错误次数过多')
                    exit()
        elif UserName in AdminUser:#判断是不是管理员用户
            while True:
                num = 0
                PassWd = PasswdNoSee('请输入密码：')
                if num <3:
                    if PassWd == AdminPassWd[AdminUser.index(UserName)]:
                        AdminOperation()
                    else:
                        print('密码错误')
                        num += 1
                else:
                    print('密码错误太多！')
                    exit()
        else:
            while True:
                UserChoose = input('用户不存在是否创建？（y/n）').strip()#用户不存在，创建用户
                if UserChoose == '是' or UserChoose.lower() == 'y':
                    while True:
                        PassWd1 = PasswdNoSee('请输入密码：')
                        PassWd2 = PasswdNoSee('\n请再次输入密码：')
                        if PassWd1 == PassWd2:
                            PassWd = PassWd2
                            print('\n创建成功！')
                            InputUserMoney()
                            shopping()
                            UserInfoSave(UserName)
                        else:
                            print('两次输入密码不相等，请重新输入')
                            continue
                elif UserChoose.lower() == 'n':
                    print("您已退出!")
                    exit()
                else:
                    print('输入有误')
def InputUserMoney():
    global UserMoney#去除数字中间的空格，感觉有点多余
    while 1 :
        money = input('请输入你的money:')
        DelSpace = []
        for i in money:
            if i == " ":
                pass
            else:
                DelSpace.append(i)
        money = ''.join(DelSpace)
        if money.isdigit():
            UserMoney += int(money)
            break
        else:
            print('请输入正确的数字')
def PrintShop():#打印商品列表
    global NumList
    global ShopName
    global ShopPrice
    num = 1
    form = PrettyTable(['编号','商品','价格','余量']) #定义表头
    for i in ShopList: #定义表格内容
        if ShopNum[num - 1] > 0 :
            form.add_row([num,i,ShopList[i],ShopNum[num - 1]])
            ShopName.append(i)
            ShopPrice.append(ShopList[i])
            NumList.append(num)
            num += 1
        else:
            form.add_row([num, i, ShopList[i],'已销售完'])
            ShopName.append(i)
            ShopPrice.append(ShopList[i])
            NumList.append(num)
            num += 1
    form.align = 'l' #所有内容左对齐
    print(form)
def PrintUserShop():
    num = 0
    form = PrettyTable(['编号','商品','购买时间',]) #和上面PrintShop一样的逻辑
    for i in UserShopList:
        form.add_row([num+1,i,ByShopTime[num]])
        num += 1
    form.align = 'l'
    print(form)
    print('余额%d'%UserMoney)
def PrintUserList():
    UserList = []  # 所有用户
    for i in AdminUser:
        UserList.append(i)
    for FileName in os.listdir():
        if '_date.txt' in FileName:
            if FileName[0:FileName.index('_')] not in UserList:
                UserList.append(FileName[0:FileName.index('_')])
    num = 0
    form = PrettyTable(['编号', '用户名'])  # 定义表头
    for i in UserList:
        form.add_row([num+1,UserList[num]])
        NumList.append(num+1)
        num += 1
    form.align = 'l'  # 所有内容左对齐
    print(form)
    return NumList
def shopping():
    global UserMoney
    global UserShopList
    global ShopName
    global ShopPrice
    global ByShopTime
    while 1 :
        print("欢迎光临小张购物车".center(50, '*'))
        ReadShop()
        PrintShop()
        print("请输入您想购买的商品(编号和商品名都可)")
        print('cx.查询\tcz.充值\t(b/q).退出')
        UserChoose = input(">>>").strip()
        if UserChoose .isdigit():
            UserChoose = int(UserChoose)
            while True:
                if UserChoose in NumList:
                    print("请输入购买数量")
                    UserChoose2 = input(">>>")
                    if UserChoose2.isdigit():
                        UserChoose2 = int(UserChoose2)
                        if ShopPrice[NumList.index(UserChoose )]*UserChoose2 <= UserMoney:
                            print(('您已成功购买%s%d个')%(ShopName[NumList.index(UserChoose )],UserChoose2))
                            UserMoney = UserMoney - ShopPrice[NumList.index(UserChoose )]
                            for i in range(0,UserChoose2):
                                UserShopList.append(ShopName[NumList.index(UserChoose )])
                                ByShopTime.append(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
                                ShopNum[NumList.index(UserChoose)]= ShopNum[NumList.index(UserChoose)] - 1
                            break
                        else:
                            print('余额不足，是否氪金？(y/n)')
                            UserChoose = input('>>>').strip()
                            if UserChoose.lower() == 'y' or UserChoose == '是' or UserChoose == '氪金':
                                print('请充值')
                                InputUserMoney()
                            elif UserChoose.lower() == 'n' or UserChoose == '否':
                                ReallyWantExit()
                    else:
                        print("输入有误!!")
                else:
                    print("请输入数字")
        elif UserChoose in ShopName:
            while True:
                print("请输入购买数量")
                UserChoose2 = input('>>>').strip()
                if UserChoose2.isdigit():
                    UserChoose2 = int(UserChoose2)
                    if  ShopPrice[ShopName.index(UserChoose )] <= UserMoney:
                        print('您已成功购买%s%d个') % (UserChoose,UserChoose2)
                        num = 0
                        if num < UserChoose2:
                            UserShopList.append(UserChoose)
                            UserMoney = UserMoney - ShopPrice[ShopName.index(UserChoose)]
                            ByShopTime.append(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
                            ShopNum[ShopName.index(UserChoose)] = ShopNum[ShopName.index(UserChoose)] - 1
                            num += 1
                            break
                    else:
                        UserChoose = input('余额不足，是否氪金？(y/n)')
                        if UserChoose.lower() == 'y' or UserChoose == '是' or UserChoose == '氪金':
                            print('请充值')
                            InputUserMoney()
                        elif UserChoose.lower() == 'n' or UserChoose == '否':
                            ReallyWantExit()
                else:
                    print('请输入数字')
        elif UserChoose == '查询' or UserChoose == 'cx':
            if UserShopList:
               PrintUserShop()
            else:
                print('您什么都没买，查询什么嘛')
        elif UserChoose == '氪金' or UserChoose == '充值' or UserChoose == '充钱' or UserChoose == 'cz' or UserChoose == 'cq' or UserChoose == 'kj':
            InputUserMoney()
        elif UserChoose.lower() == 'b' or UserChoose.lower() == 'q':
            ReallyWantExit()
        else:
            print("输入有误！")
def UserInfoSave(name):
    if UserName in AdminUser:
        if SaveChoose == '1' or '测试':
            name = name +'AdminTest'
            UserInfo = open('%s_date.txt'%(name),mode = 'w',encoding='utf-8')  #新建用户日志文件
            for i in UserShopList:  #第一行已买商品
                UserInfo.write('%s,'%i)
            UserInfo.write('\n') #换行
            for i in ByShopTime: #第二行购买商品时间
                UserInfo.write('%s,' % i)
            UserInfo.write('\n%s\n'%UserMoney) #第三行用户余额
            UserInfo.close()
            print('用户信息已经保存')
        else:
            UserInfo = open('%s_date.txt' % (name), mode='w', encoding='utf-8')  # 新建用户日志文件
            for i in UserShopList:  # 第二行已买商品
                UserInfo.write('%s,' % i)
            UserInfo.write('\n')  # 换行
            for i in ByShopTime:  # 第三行购买商品时间
                UserInfo.write('%s,' % i)
            UserInfo.write('\n%s\n' % UserMoney)  # 第四行用户余额
            UserInfo.close()
            print('用户信息已经保存')
    else:
        UserInfo = open('%s_date.txt' % (name), mode='w', encoding='utf-8')  # 新建用户日志文件
        UserInfo.write("%s\n" % PassWd)  # 第一行密码
        for i in UserShopList:  # 第二行已买商品
            UserInfo.write('%s,' % i)
        UserInfo.write('\n')  # 换行
        for i in ByShopTime:  # 第三行购买商品时间
            UserInfo.write('%s,' % i)
        UserInfo.write('\n%s\n' % UserMoney)  # 第四行用户余额
        UserInfo.close()
        print('用户信息已经保存')
def ReallyWantExit():#确认用户是否真的想退出
    if UserShopList:#如果有买的东西
        print('您已购买：')
        PrintUserShop()
        print('您还剩%s' % UserMoney)
        UserInfoSave(UserName)
        SaveShop()
        exit()
    else: #什么都没买
        while True:
            print('你什么都没有买真的舍得吗？')
            print('1.否（充值）\t2.是(退出)')
            UserChoose = input('>>>')
            if UserChoose.lower() == 'y' or UserChoose == '是' or UserChoose == '2':
                print('嘤嘤嘤，你点都不爱我，我哭了/(ㄒoㄒ)/')
                exit()
            elif UserChoose.lower() == 'n' or UserChoose == '否' or UserChoose == '1':
                print('那就氪金吧！')
                InputUserMoney()
                shopping()
            else:
                print("输入有误！！！")
def SaveShop():
    ShopInfo = open('ShopList.txt', mode='w', encoding='utf-8')  # 更新商品数量
    num = 0
    for i in ShopList:
        ShopInfo.write(('%s:%s,%s\n') % (i, ShopList[i], ShopNum[num]))
        num += 1
    print('商品已保存！')
    ShopInfo.close()
    exit()
def SaveAdmin():
    UserInfo = open('Admin.txt', mode='w', encoding='utf-8')
    for i in AdminUser:
        UserInfo.write('%s:'%i)
        UserInfo.write('%s\n'%AdminPassWd[AdminUser.index(i)])
    print('管理员信息已保存')
UserChoose()
