import sys
import os
goods = [
{"name": "电脑", "price": 1999},
{"name": "鼠标", "price": 10},
{"name": "游艇", "price": 20},
{"name": "美女", "price": 998},
]
user_file = ['Alex','WuSir','太白','李露']
password_flie = ['Alex','WuSir','太白','李露']
goods_name = []
goods_price = []
goods_number = []
user_goods = []
def purchase():
    global goods_name
    global goods_price
    global goods_number
    global user_money
    global user_goods
    while 1:
        number_or_name = input('\033[0m>>请输入您想购买的东西的编号/名称(输入q退出)：').strip()
        if number_or_name.isdigit() == True :
            number_or_name = int(number_or_name)
            if number_or_name in goods_number :
                number1 = goods_number.index(number_or_name)
                number2 = goods_price[number1]
                user_money -= number2
                if user_money >= 0 :
                    print(('\033[1;31m>>购买成功，您的余额为%d\033[0m')%(user_money))
                    user_buy_goods = goods_name[number1]
                    user_goods.append(user_buy_goods)
                else:
                    print('>>购买失败，余额不足')
            else:
                 print('>>编号输入错误，请核对后再试')
        elif number_or_name in goods_name :
            number = goods_name.index(number_or_name)
            number = goods_price[number]
            user_money = user_money - number
            if user_money >= 0 :
                print(('>>\033[1;34m您已成功购买%s，您的余额为%d\033[0m')%(goods_name[number1],user_money))
                print_goods()
                user_buy_goods = goods_name[number1]
                user_goods.append(user_buy_goods)
            else:
                print('\033[1;31m>>购买成功，但您的余额以为负，请重新登陆充值后购买\033[0m')
        elif number_or_name.lower() == 'q':
            print('\033[1;31m>>您已经退出该程序\033[0m')
            break
        else:
            print('>>请输入编号/商品名')
def login(a,b):
    global user_file
    global password_flie
    global username
    global goods_name
    global goods_price
    global goods
    global goods_number
    global user_money
    time = 1
    while 1:
        for i in user_file:
            print(i)
        user_choose = input('>>\033[0m是否使用上面账号登陆y/n：').strip()
        if user_choose.lower() == 'y':
            username = input(('>>请输入%s:')%(a)).strip()
            if time <  5:
                if username in user_file :
                    user_index = user_file.index(username)
                    password_upper = password_flie[user_index].upper()
                    password = input(('>>请输入%s') % (b)).strip().upper()
                    if password == password_upper:
                        print('>>登陆成功')
                        if os.path.exists('%s_date.txt'%(username)) == True:
                            user_date = open('%s_date.txt'%(username),mode='r',encoding='utf-8')
                            date = user_date.readlines()
                            user_goods = date[1].strip('_')
                            user_money = date[2]
                            user_money = int(user_money)
                            user_date.close()
                            print(('您的余额为：%d')%(user_money))
                            print('您上次购买了以下商品：')
                            print(('%s')%(user_goods))
                            print_goods()
                            user_choose = input('是否充值？y/n：')
                            if user_choose.upper() == 'Y':
                                while 1:
                                    new_money = input('请输入充值金额：').strip()
                                    if new_money.isdigit() == True :
                                        new_money = int(new_money)
                                        user_money += new_money
                                        break
                                    else:
                                        print('请输入正整数')
                            purchase()
                            break
                        else:
                            input_user_money()
                            print_goods()
                            purchase()
                            break
                    else:
                        print('>>\033[1;31m密码错误\033[0m')
                        time += 1
                else:
                    print('>>\033[1;31m用户不存在\033[0m')
            elif time == 5:
                print('>>\033[1;31m错误次数过多，请稍后再试\033[0m')
                sys.exit()
        elif user_choose.lower() == 'n':
            user_name = input('请输入您想创建的用户名：')
            if user_name in user_file:
                print('\033[1;31m该账号已经存在，请直接登陆\033[om')
                break
            else:
                user_file.append(user_name)
                c = True
                while c:
                    first_pwd = input('请输入密码:')
                    second_pwd = input('请再次输入密码：')
                    if first_pwd == second_pwd:
                        print('\033[1;31m用户创建成功，请登陆\033[0m')
                        password_flie.append(second_pwd)
                        c = True
                    else:
                        print('\033[1;31m两次密码不相同，请核对后再试\033[0m')
        else:
            print('\033[1;31m>>请输入y/n\033[0m')
def storage(a):
    global user_goods
    global user_money
    user_goods_flie = '_'.join(user_goods)
    user_data = open('%s_date.txt'%(a),mode = 'w',encoding='utf-8')
    user_data.write(('%s\n%s\n%s')%(a,user_goods,user_money))
    user_data.close()
    print('>>您的用户数据已存储')
def generate_goods_list():
    global goods_name
    global goods_price
    global goods
    global goods_number
    time = 1
    for i in goods:
        goods_name.append(i.get('name'))
        goods_price.append(i.get('price'))
        goods_number.append(time)
        time += 1
def print_goods():
    global goods
    print('>>您的购物车共有以下商品：\033[1;34m')
    for i in goods:
        print(goods.index(i)+1 ,i.get('name'),i.get('price'))
def input_user_money():
    global user_money
    while 1 :
        user_money = input('>>请输入您的工资：').strip()
        if user_money.isdigit() == True:
            user_money = int(user_money)
            print(('>>您的工资为%d')%(user_money))
            break
        else:
            print('>>请输入正数')
print('\n\033[1;35m******************欢迎进入小张购物车******************\033[0m')
print('>>此次购物车使用共有以下人员\n\033[1;31m注意!默认存在的用户名同密码一样,密码不区分大小写\n您也可以自己创建用户\033[1;36m')
generate_goods_list()
login('用户名','密码')
print('>>\033[1;34m您已购买以下东西:')
for i in user_goods:
    print(i)
print(('\033[1;34m>>您的余额为：%d')%(user_money))
storage(('%s')%(username))









    

