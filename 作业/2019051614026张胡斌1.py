#!/usr/bin/env python
#-*- coding:utf-8 -*-
from prettytable import PrettyTable
import datetime
import os
UserShopList = [] #用户已买列表
ShopName = [] #商品名字
ShopPrice = [] #商品价格
NumList = [] #编号
ByShopTime = [] #商品购买时间
ShopNum = [] #商品数量
AdminUser = [] #管理员用户
AdminPassWd = [] #管理员密码
UserList = [] #用户列表
UserNumList = []#用户编号列表
UserMoney = [] #用户金钱
UserName = [] #当前用户名
PassWd = [] #当前密码
def ReadAdmin():#读取管理员账户以及密码
    UserInfo = open('Admin.txt',mode='r',encoding='utf-8')
    for lines in UserInfo:
        AdminUser.append(lines[0:lines.index(':')])
        AdminPassWd.append(lines[(lines.index(':')+1):].strip())
    return AdminUser,AdminPassWd
def ReadShop():#读取商品列表
    ReadShopList = open('ShopList.txt',mode='r',encoding='utf-8')
    for lines in ReadShopList:
        lines.strip()
        key = lines[0:lines.index(':')]
        value = lines[(lines.index(':')+1):lines.index(',')]
        ShopName.append(key)
        ShopPrice.append(value)
        ShopNum.append(int(lines[lines.index(',') + 1:].strip()))
    return ShopNum,ShopPrice,ShopName
def AdminChoose(): #管理员选择操作
    print('欢迎管理员%s进入'%UserName[0])
    while True:
        print("请选择您的身份：")
        print("1.管理员\t2用户")
        Choose = input(">>>")
        if Choose == "1" or Choose == "管理员":
            print('1.修改密码\t2.修改商品\t3.退出（b）')
            AdminChoose = input('请输入您要进行的操作：').strip() #管理员选择操作
            if AdminChoose == '1' or AdminChoose =='修改密码' :
                return 1
            elif AdminChoose == '2' or AdminChoose == '修改商品':
                return 2
            elif AdminChoose == '3' or AdminChoose == '退出' or  AdminChoose.lower() == 'b':
                print('您已退出')
                SaveShop()
                exit()
                return 3
            else:
                print("输入有误！")
        elif Choose == "2" or Choose == "用户":
            if os.path.exists('%s_date.txt' % (UserName[0])):
                with open('%s_date.txt' % (UserName[0]), mode="r", encoding="utf-8") as f:
                    ShopList = f.readline().strip().split(',')  # 读取已买商品名
                    ShopList.pop()
                    for i in ShopList:
                        UserShopList.append(i)
                    ShopTime = f.readline().strip().split(',')  # 读取已买商品时间
                    ShopTime.pop()
                    for i in ShopTime:
                        ByShopTime.append(i)
                    UserMoney.append(int(f.readline()))  # 读取用户余额
                return 4 ,UserShopList,ByShopTime,UserMoney#管理员以普通用户身份登录
            else:
                return 4
        elif Choose.lower() == "b":
            print("您已退出")
            SaveShop()
            exit()
def ChangeGoods(): #修改商品
    while True:
        Result = PrintShop()
        print("可以根据序号或者商品名选择修改商品名字，价格或者数量")
        print('输入增加或者删除用于增加或者删除商品')
        print('可以退出或者返回上一层')
        AdminChoose = input(">>>").strip()
        if AdminChoose.isdigit():
            AdminChoose = int(AdminChoose)
            if AdminChoose in NumList:
                while True:
                    print("您已经选择商品%s" % ShopName[NumList.index(AdminChoose)])
                    print("请选择您接下来的操作：\n1.修改名字\n2.修改价格\n3.修改余量")
                    while True:
                        AdminChoose2 = input('>>>').strip()
                        if AdminChoose2 == '修改名字' or AdminChoose2 == '1':
                            while True:
                                print("您想将%s修改成什么？" % ShopName[NumList.index(AdminChoose)])
                                AdminChoose3 = input('>>>').strip()
                                print(('是否将%s修改成%s') % (ShopName[NumList.index(AdminChoose)], AdminChoose3))
                                while True:
                                    AdminChoose4 = input(">>>").strip()
                                    if AdminChoose4.lower() == 'y' or AdminChoose4 == '是':
                                        ShopName[NumList.index(AdminChoose)] = AdminChoose3
                                        print("修改成功！")
                                        return ShopName
                                    elif AdminChoose4.lower() == 'n' or AdminChoose4 == '否':
                                        break
                                    else:
                                        print("输入有误！")
                                break
                        elif AdminChoose2 == '修改价格' or AdminChoose2 == '2':
                            while True:
                                print(("商品%s价格%s") % (ShopName[NumList.index(AdminChoose)], ShopPrice[NumList.index(AdminChoose)]))
                                print("您想修改成多少钱？")
                                AdminChoose3 = input('>>>').strip()
                                if AdminChoose3.isdigit():
                                    while True:
                                        print("是否修改为%d元" % int(AdminChoose3))
                                        AdminChoose4 = input(">>>").strip()
                                        if AdminChoose4.lower() == 'y' or AdminChoose4 == '是':
                                            print('操作成功')
                                            ShopPrice[NumList.index(AdminChoose)] = AdminChoose3
                                            return ShopPrice
                                        elif AdminChoose4.lower() == 'n' or AdminChoose4 == '否':
                                            break
                                        else:
                                            print("输入有误！")
                                else:
                                    print("请输入数字")
                                    continue
                                break
                        elif AdminChoose2 == '修改余量' or AdminChoose2 == '3':
                            while True:
                                print(("商品%s余量%d") % (
                                ShopName[NumList.index(AdminChoose)], ShopNum[NumList.index(AdminChoose)]))
                                print("您想修改成多少？")
                                AdminChoose3 = input('>>>').strip()
                                if AdminChoose3.isdigit():
                                    while True:
                                        print('是否修改为%d?' % int(AdminChoose3))
                                        AdminChoose4 = input(">>>").strip()
                                        if AdminChoose4.lower() == 'y' or AdminChoose4 == '是':
                                            ShopNum[NumList.index(AdminChoose)] = int(AdminChoose3)
                                            print("操作成功！")
                                            return ShopNum
                                else:
                                    print("请输入数字！")
                                    continue
                                break
                        else:
                            print("输入有误！")
                            continue
                        break
                    break
            else:
                print('\n没有您输入的序号')
        elif AdminChoose in ShopName:
            while True:
                print("您已经选择商品%s" % AdminChoose)
                print("请选择您接下来的操作：\n1.修改名字\n2.修改价格\n3.修改余量")
                while True:
                    AdminChoose2 = input('>>>').strip()
                    if AdminChoose2 == '修改名字' or AdminChoose2 == '1':
                        while True:
                            print("您想将%s修改成什么？")
                            AdminChoose3 = input('>>>').strip()
                            print(('是否将%s修改成%s') % (AdminChoose, AdminChoose3))
                            AdminChoose4 = input(">>>").strip()
                            if AdminChoose4.lower() == 'y' or AdminChoose4 == '是':
                                ShopName[ShopName.index(AdminChoose)] = AdminChoose3
                                print("修改成功！")
                                return ShopName
                            else:
                                print("输入有误！")
                    elif AdminChoose2 == '修改价格' or AdminChoose2 == '2':
                        while True:
                            print(("商品%s价格%d") % (AdminChoose, ShopPrice[ShopName.index(AdminChoose)]))
                            print("您想修改成多少钱？")
                            AdminChoose3 = input('>>>').strip()
                            if AdminChoose3.isdigit():
                                while True:
                                    print("是否修改为%d元" % int(AdminChoose3))
                                    AdminChoose4 = input(">>>").strip()
                                    if AdminChoose4.lower() == 'y' or AdminChoose4 == '是':
                                        print('操作成功')
                                        ShopPrice[ShopName.index(AdminChoose)] = int(AdminChoose3)
                                        return ShopPrice
                                    else:
                                        print("输入有误！")
                            else:
                                print("请输入数字")
                                continue
                            break
                    elif AdminChoose2 == '修改余量' or AdminChoose2 == '3':
                        while True:
                            print(("商品%s余量%d") % (AdminChoose, ShopNum[ShopName.index(AdminChoose)]))
                            print("您想修改成多少？")
                            AdminChoose3 = input('>>>').strip()
                            if AdminChoose3.isdigit():
                                while True:
                                    print('是否修改为%d?' % int(AdminChoose3))
                                    AdminChoose4 = input(">>>").strip()
                                    if AdminChoose4.lower() == 'y' or AdminChoose4 == '是':
                                        ShopNum[ShopName.index(AdminChoose)] = AdminChoose3
                                        print("操作成功！")
                                        return ShopNum
                            else:
                                print("请输入数字！")
                                continue
                            break
                    else:
                        print('输入有误！')
                        continue
                    break
        elif AdminChoose == '增加':
            while True:
                print('请输入您想增加的商品：')
                AdminChoose2 = input(">>>").strip()
                while True:
                    print('请输入%s的价格：' % AdminChoose2)
                    AdminChoose3 = input(">>>").strip()
                    if AdminChoose3.isdigit():
                        while True:
                            print('请输入%s的余量：' % AdminChoose2)
                            AdminChoose4 = input(">>>").strip()
                            if AdminChoose4.isdigit():
                                ShopName.append(AdminChoose2)
                                ShopPrice.append(int(AdminChoose3))
                                ShopNum.append(AdminChoose4)
                                print("操作成功！")
                                return ShopName, ShopPrice, ShopNum
                            else:
                                print('请输入数字！')
                    else:
                        print("请输入数字！")
                        continue
                    break
                break
        elif AdminChoose == '删除':
            while True:
                print('请输入您要删除的商品名或者编号')
                AdminChoose2 = input('>>>').strip()
                if AdminChoose2.isdigit():
                    AdminChoose2 = int(AdminChoose2)
                    if AdminChoose2 in NumList:
                        while True:
                            print('是否删除商品%s？' % ShopName[NumList.index(AdminChoose2)])
                            AdminChoose3 = input('>>>').strip()
                            if AdminChoose3.lower() == 'y' or AdminChoose3 == '是':
                                ShopName.pop(NumList.index(AdminChoose2))
                                ShopPrice.pop(NumList.index(AdminChoose2))
                                ShopNum.pop(NumList.index(AdminChoose2))
                                print("操作成功！")
                                return ShopName, ShopPrice, ShopNum
                            elif AdminChoose3.lower() == 'n' or AdminChoose3 == '否':
                                print("好的")
                                break
                            else:
                                print('输入有误')
                    else:
                        print('请输入正确的编号！')
                elif AdminChoose2 in ShopName:
                    while True:
                        print('是否删除商品%s？' % AdminChoose2)
                        AdminChoose3 = input('>>>').strip()
                        if AdminChoose3.lower() == 'y' or AdminChoose3 == '是':
                            ShopPrice.pop(ShopName.index(AdminChoose2))
                            ShopNum.pop(ShopName.index(AdminChoose2))
                            ShopName.pop(ShopName.index(AdminChoose2))
                            print("操作成功！")
                            return ShopPrice, ShopNum, ShopName
                        elif AdminChoose3.lower() == 'n' or AdminChoose3 == '否':
                            print("好的")
                            break
                        else:
                            print('输入有误')
                else:
                    print("输入有误！")
                    continue
                break
        elif AdminChoose.lower() == 'b' or AdminChoose == '上一步':
            return "b"
        elif AdminChoose == '退出':
            SaveAdmin()
            SaveShop()
            exit()
        else:
            print('输入有误！')
def UserChoose():  #开始让用户选择是否直接进入购物车程序还是登陆
    ReadAdmin()
    while True:
        print("是登陆还是直接进入?")
        print('1.登录\t2.进入')
        UserChoose = input(">>>").strip()
        if UserChoose == '登陆' or UserChoose == '登录' or UserChoose == '1' :
            return 1
        elif UserChoose == '进入'or UserChoose == '2':
            return 2
        else:
            print("输入有误！")
def PasswdNoSee(a):#密码不可见
    import msvcrt, os
    print(a, end='', flush=True)
    li = []
    while 1:
        ch = msvcrt.getch()
        #回车
        if ch == b'\r':
            return b''.join(li).decode() #把list转换为字符串返回
            break
        #退格
        elif ch == b'\x08':
            if li:
                li.pop()
                msvcrt.putch(b'\b')
                msvcrt.putch(b' ')
                msvcrt.putch(b'\b')
        #Esc
        elif ch == b'\x1b':
            break
        else:
            li.append(ch)
            msvcrt.putch(b'*')
    return b''.join(li).decode()##
def PrintShop():#打印商品列表
    num = 1
    form = PrettyTable(['编号','商品','价格','余量']) #定义表头
    for i in ShopName: #定义表格内容
        if ShopNum[num - 1] > 0 :
            form.add_row([num,i,ShopPrice[ShopName.index(i)],ShopNum[num - 1]])
            NumList.append(num)
            num += 1
        else:
            form.add_row([num, i, ShopPrice[ShopName.index(i)],'已销售完'])
            NumList.append(num)
            num += 1
    form.align = 'l' #所有内容左对齐
    print(form)
    return NumList
def PrintUserShop():
    num = 0
    form = PrettyTable(['编号','商品','购买时间',]) #和上面PrintShop一样的逻辑
    for i in UserShopList:
        form.add_row([num+1,i,ByShopTime[num]])
        num += 1
    form.align = 'l'
    print(form)
    print('余额%d'%UserMoney[0])
def shopping():
    while 1 :
        print("欢迎光临小张购物车".center(50, '*'))
        PrintShop()
        print("请输入您想购买的商品(编号和商品名都可)")
        print('cx.查询\tcz.充值\t(b/q).退出\tgm（修改密码）')
        UserChoose = input(">>>").strip()
        if UserChoose .isdigit():
            while True:
                UserChoose = int(UserChoose)
                if UserChoose in NumList:
                    print("请输入购买数量")
                    UserChoose2 = input(">>>")
                    if UserChoose2.isdigit():
                        UserChoose2 = int(UserChoose2)
                        if ShopNum[NumList.index(UserChoose)] >= UserChoose2:
                            if int(ShopPrice[NumList.index(UserChoose )])*UserChoose2 <= UserMoney[0]:
                                print(('您已成功购买%s%d个')%(ShopName[NumList.index(UserChoose )],UserChoose2))
                                for i in range(0,UserChoose2):
                                    UserShopList.append(ShopName[NumList.index(UserChoose )])
                                    ByShopTime.append(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
                                    ShopNum[NumList.index(UserChoose)]= ShopNum[NumList.index(UserChoose)] - 1
                                    UserMoney[-1] -= int(ShopPrice[NumList.index(UserChoose)])
                                return UserShopList,ByShopTime,ShopNum,UserMoney
                            else:
                                print('余额不足，是否氪金？(y/n)')
                                UserChoose = input('>>>').strip()
                                if UserChoose.lower() == 'y' or UserChoose == '是' or UserChoose == '氪金':
                                    print('请充值')
                                    return "充值"
                                elif UserChoose.lower() == 'n' or UserChoose == '否':
                                    return "退出"
                        else:
                            print("余量不足！请联系管理员添加！")
                    else:
                        print("输入有误!!")
                else:
                    print("请输入数字")
        elif UserChoose in ShopName:
            while True:
                print("请输入购买数量")
                UserChoose2 = input('>>>').strip()
                if UserChoose2.isdigit():
                    UserChoose2 = int(UserChoose2)
                    if ShopNum[ShopName.index(UserChoose)] >= UserChoose2:
                        if  ShopPrice[ShopName.index(UserChoose )] <= UserMoney[0]:
                            print('您已成功购买%s%d个') % (UserChoose,UserChoose2)
                            for i in range(0,UserChoose2):
                                UserShopList.append(UserChoose)
                                UserMoney[-1] -= ShopPrice[ShopName.index(UserChoose)]
                                ByShopTime.append(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
                                ShopNum[ShopName.index(UserChoose)] = ShopNum[ShopName.index(UserChoose)] - 1
                            return UserShopList, ByShopTime, ShopNum,UserMoney
                        else:
                            UserChoose = input('余额不足，是否氪金？(y/n)')
                            if UserChoose.lower() == 'y' or UserChoose == '是' or UserChoose == '氪金':
                                print('请充值')
                                return "充值"
                            elif UserChoose.lower() == 'n' or UserChoose == '否':
                                return "退出"
                    else:
                        print("余量不足，请联系管理员添加！")
                else:
                    print('请输入数字')
        elif UserChoose == '查询' or UserChoose.lower() == 'cx':
            if len(UserShopList)>0:
               return "查询"
            else:
                print('您什么都没买，查询什么嘛')
        elif UserChoose == '氪金' or UserChoose == '充值' or UserChoose == '充钱' or UserChoose.lower() == 'cz' or UserChoose.lower() == 'cq' or UserChoose.lower() == 'kj':
            return "充值"
        elif UserChoose.lower() == 'b' or UserChoose.lower() == 'q':
            return "退出"
        elif UserChoose.lower() == "gm" or UserChoose == "修改密码":
            return "改密"
        else:
            print("输入有误！")
def UserInfoSave(name):
    if UserName[0] in AdminUser:
        UserInfo = open('%s_date.txt' % (name), mode='w', encoding='utf-8')  # 新建用户日志文件
        for i in UserShopList:  # 第二行已买商品
            UserInfo.write('%s,' % i)
        UserInfo.write('\n')  # 换行
        for i in ByShopTime:  # 第三行购买商品时间
            UserInfo.write('%s,' % i)
        UserInfo.write('\n%s\n' % UserMoney[0])  # 第四行用户余额
        UserInfo.close()
        print('用户信息已经保存')
    else:
        UserInfo = open('%s_date.txt' % (name), mode='w', encoding='utf-8')  # 新建用户日志文件
        UserInfo.write("%s\n" % PassWd[0])  # 第一行密码
        for i in UserShopList:  # 第二行已买商品
            UserInfo.write('%s,' % i)
        UserInfo.write('\n')  # 换行
        for i in ByShopTime:  # 第三行购买商品时间
            UserInfo.write('%s,' % i)
        UserInfo.write('\n%s\n' % UserMoney[0])  # 第四行用户余额
        UserInfo.close()
        print('用户信息已经保存')
def ReallyWantExit():#确认用户是否真的想退出
    if UserShopList:#如果有买的东西
        print('您已购买：')
        PrintUserShop()
        print('您还剩%s' % UserMoney)
        UserInfoSave(UserName)
        SaveShop()
        exit()
    else: #什么都没买
        while True:
            print('你什么都没有买真的舍得吗？')
            print('1.否（充值）\t2.是(退出)')
            UserChoose = input('>>>')
            if UserChoose.lower() == 'y' or UserChoose == '是' or UserChoose == '2':
                print('嘤嘤嘤，你点都不爱我，我哭了/(ㄒoㄒ)/')
                exit()
            elif UserChoose.lower() == 'n' or UserChoose == '否' or UserChoose == '1':
                print('那就氪金吧！')
                return "充值"
            else:
                print("输入有误！！！")
def SaveShop():
    ShopInfo = open('ShopList.txt', mode='w', encoding='utf-8')  # 更新商品数量
    for i in ShopName:
        ShopInfo.write(('%s:%s,%s\n') % (i, ShopPrice[ShopName.index(i)], ShopNum[ShopName.index(i)]))
    print('商品已保存！')
    ShopInfo.close()
    exit()
def UserLogin():#用户登录
    while True:
        Name = input("请输入用户名：")
        if Name in AdminUser:
            mima = AdminPassWd[AdminUser.index(Name)]
            num = 0
            while True:
                Password = PasswdNoSee("请输入密码:")
                print("\n")
                if num < 3:
                    if mima == Password:
                        PassWd.append(mima)
                        UserName.append(Name)
                        return 1,UserName,PassWd # 代表管理员用户
                    else:
                        print("密码错误")
                        num += 1
                else:
                    print("错误次数过多！")
                    exit()
        elif os.path.exists('%s_date.txt' % (Name)):
            with open('%s_date.txt' % (Name),mode="r",encoding="utf-8") as f:
                mima = str(f.readline()).strip()  # 读取密码
                ShopList = f.readline().strip().split(',')  # 读取已买商品名
                ShopList.pop()
                for i in ShopList:
                    UserShopList.append(i)
                ShopTime = f.readline().strip().split(',')  # 读取已买商品时间
                ShopTime.pop()
                for i in ShopTime:
                    ByShopTime.append(i)
                UserMoney.append(int(f.readline()))  # 读取用户余额
            num = 0
            while True:
                Password = PasswdNoSee("请输入密码")
                print("\n")
                if num < 3:
                    if  mima == Password:
                        UserName.append(Name)
                        PassWd.append(mima)
                        return 2,ByShopTime,UserMoney,PassWd,UserName,UserShopList#代表普通用户
                    else:
                        print("密码错误")
                        num += 1
                else:
                    print("错误次数过多！")
                    exit()
        else:
            while True:
                print(("用户%s不存在，是否创建？(y/n)")%(Name))
                Choose = input(">>>")
                if Choose == "是" or Choose.lower() == "y":
                    print(("用户名：%s")%(Name))
                    while True:
                        Password1 = PasswdNoSee("请输入密码：")
                        print("\n")
                        Password2 = PasswdNoSee("请确认密码：")
                        print("\n")
                        if Password1 == Password2:
                            print(("用户%s创建成功")%(Name))
                            UserName.append(Name)
                            PassWd.append(Password1)
                            return 2,UserName,PassWd
                        else:
                            print("请重新输入！")
                elif Choose == "否" or Choose.lower() == "n":
                    print("再见？")
                    exit()
                else:
                   print("输入有误！")
def ChangePasswd():
    while True:
        PasswordNow = PasswdNoSee("请输入当前密码：")
        print("\n")
        if  PasswordNow == PassWd[0]:
            while True:
                PasswordNew = PasswdNoSee("请输入新密码：")
                print("\n")
                PasswordNew1 = PasswdNoSee("请确认新密码：")
                if PasswordNew == PasswordNew1:
                    print("\n")
                    PassWd[0] = PasswordNew
                    print("密码修改成功！")
                    return "b",PassWd
                else:
                    print("两次密码不相等！！！")
        else:
            print("密码错误")
def InputUserMoney():
    while True:
        a = input("请输入您的money").strip()
        if a.isdigit():
            a = int(a)
            if len(UserMoney)>0:
                UserMoney.append(UserMoney[0] + a)
            else:
                UserMoney.append(a)
            return UserMoney
        else:
            print("请输入数字！！")
def Main():
    ReadShop()
    ReadAdmin()
    Result = UserChoose()
    if 1 == Result:
        Result = UserLogin()
        if 1 in Result:   #管理员
            while True:
                Result = AdminChoose()
                while True:
                    if 1 == Result:
                        while True:
                            Result = ChangePasswd()
                            SaveAdminPasswd()
                            if "b" in Result:
                                break
                    elif 2 == Result:
                        while True:
                            Result = ChangeGoods()
                            if "b" in Result:
                                break
                    elif 3 == Result:
                        SaveShop()
                    elif 4 in Result:
                        if UserMoney[0]:
                            while True:
                                Result = shopping()
                                if "退出" in Result:
                                    UserInfoSave(UserName[0])
                                    SaveShop()
                                    exit()
                                elif "充值" in Result:
                                    InputUserMoney()
                                elif "查询" == Result:
                                    PrintUserShop()
                                elif "改密" == Result:
                                    ChangePasswd()
                                    SaveAdminPasswd()
                        else:
                            while True:
                                Result = shopping()
                                if "退出" in Result:
                                    UserInfoSave(UserName[0])
                                    SaveShop()
                                    exit()
                                elif "充值" in Result:
                                    InputUserMoney()
                                elif "查询" == Result:
                                    PrintUserShop()
                                elif "改密" == Result:
                                    ChangePasswd()
                                    SaveAdminPasswd()
                    break
        elif 2 in Result: #用户登录
            if UserMoney != []:
                while True:
                    Result = shopping()
                    if "退出" in Result:
                        UserInfoSave(UserName[0])
                        SaveShop()
                        exit()
                    elif "充值" in Result:
                        InputUserMoney()
                    elif "查询" == Result:
                        PrintUserShop()
                    elif "改密" == Result:
                        ChangePasswd()
            else:
                InputUserMoney()
                while True:
                    Result = shopping()
                    if "退出" in Result:
                        UserInfoSave(UserName[0])
                        SaveShop()
                        exit()
                    elif "充值" in Result:
                        InputUserMoney()
                    elif "查询" == Result:
                        PrintUserShop()
                    elif "改密" == Result:
                        ChangePasswd()
    elif Result == 2:
        print("欢迎光临小张购物车".center(50, '*'))
        PrintShop()
        print("请输入您想购买的商品(编号和商品名都可)")
        print('cx.查询\tcz.充值\t(b/q).退出\tgm（修改密码）')
        while True:
            input(">>>")
            print("您还未登陆，是否登录？(y/n)")
            Choose = input(">>>")
            if Choose == "是" or Choose.lower() == "y":
                Main()
            elif Choose == "否" or Choose.lower() == "n":
                print("再见？")
                exit()
            else:
                print("输入有误")
def SaveAdminPasswd():#修改储存管理员修改密码
    with open('Admin.txt',mode='w',encoding='utf-8') as f:
        for i in AdminUser:
            if i == UserName[0]:
                f.write(("%s:%s\n")%(i,PassWd[0]))
            else:
                f.write(("%s:%s\n")%(i,AdminPassWd[AdminUser.index(i)]))
Main()