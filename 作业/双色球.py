# -*- coding:utf-8 -*-
#/usr/bin/python
print('欢迎来到小张双色球选购程序')
print('本次共需要您选择红球6个，蓝球2个,红球范围1-32，蓝球范围1-16')
red_ball = []
blue_ball = []
def red_ball_choose():
    global red_ball
    ball_choose_number = 0
    while ball_choose_number < 6 :
            ball_number = input(('\033[1;31m请输入您购买的%d号红球的号数:\033[1;31m')%( ball_choose_number + 1 ))
            if ball_number.isdigit() == True :
                ball_number = int(ball_number)
                if ball_number > 0 and ball_number < 33 :
                    if ball_number in red_ball :
                        print('这个球已经有了')
                    else:
                        red_ball.append(ball_number)
                        ball_choose_number += 1
                else :
                    print('红球只能选择1-32中的任何一个')
            else:
                print('请输入正整数')
def blue_ball_choose():
    global blue_ball
    ball_choose_number = 0
    while ball_choose_number < 2 :
            ball_number = input(('\033[1;34m请输入您购买的%d号蓝球的号数:\033[1;34m') % (ball_choose_number + 1))
            if ball_number.isdigit() == True:
                ball_number = int(ball_number)
                if ball_number > 0 and ball_number < 17:
                    if ball_number in blue_ball:
                        print('这个球已经有了')
                    else:
                        blue_ball.append(ball_number)
                        ball_choose_number += 1
                else:
                    print('蓝球只能选择1-16中的任何一个')
            else:
                print('请输入正整数')
red_ball_choose()
blue_ball_choose()
print(('''\n\n\n\n\033[1;31m红球:%s\033[1;31m''')%(red_ball))
print(('\033[1;34m蓝球：%s\033[1;34m')%(blue_ball))
print('\033[0m祝你好运\033[0m ')