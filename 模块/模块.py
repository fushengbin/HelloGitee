#!/usr/bin/env python 
#-*- coding:utf-8 -*-
'''
模块基础知识：
模块分为三类：
内置模块
第三方模块
自定义模块
'''
import sys
import os
import shutil
import json
import pickle
import time
from datetime import datetime,timedelta,timezone
# sys.getrefcount() #计算某个变量引用的次数
# sys.stdout.write()#和print差不多，打印只不过print自带换行符那种
# 如：
print(1230)
print(1236)
print('----------分割符----------')
sys.stdout.write('32124')
sys.stdout.write('24141')
# 补充一个占位符的知识：\r 回到当前行起始位置，但是要加'end = '
file_size = os.stat('文件名').st_size#读取文件大小，字节
sys.argv #获取用户输入后面的东西，即你在命令行运行这个脚本后面传入的参数，默认会将其保存到一个列表中，列表的第一个是该脚本的路径
shutil.rmtree()#删除某个目录 shutil模块下
os.path.exists()#判断文件是否存在，存在返回Ture，不存在false
os.path.abspath()#查找文件的绝对路径
os.path.dirname()#获取文件的上级目录
# 在字符串前面加一个r，转义该字符串
os.path.join()#路径拼接
os.listdir()#查看某个目录下所有文件（就是该文件这层的所有东西）
os.walk()#查看所有文件
os.mkdir()#生成文件夹（一层）
os.makedirs()#生成文件夹（多层）
os.rename()#重命名
sys.path()#默认python找模块的时候查找的地方，也就是python模块的环境变量
from 模块名 import 函数名之类的 as 变量名  #导入某个模块中的某个功能做为变量名那个东西
from 模块名 impor *
v = [213,4312,'key'= 'z',True,'DSADSA']
v1 = json.dumps(v)#将python的值转化成json格式的字符串（序列化）
v2 = json.load(v1)
_file_#特殊字符串，当前运行文件的参数
os.path.abspath(_file_)#当前文件绝对路径
# json 和pickle 的去呗
# json：优点：所有语言通用；缺点：只能序列化python的基础数据类型
# pickle:优点:能序列化python种的所有数据类型,缺点:序列化的内容就只有python能识别
# pickle序列化:
v = {1,2,3,4}
v1 = pickle.dumps(v) #序列化
v2 = pickle.loads(v1)#反序列化
shutil.move()#重命名，第一个是源文件名，第二个是想修改的重命名
shutil.make_archive()#压缩文件，三个参数，第一个压缩后文件名，第二个压缩格式，第三个要压缩的东西
shutil.unpack_archive('',extract_dir=,format=)#解压文件，第一个是解压的文件，第二个是保存的位置(如果保存的位置不存在，则自动生成)，第三个是压缩格式
datetime.now().strftime('时间格式化字符')#这里可以对时间进行自定义的格式,并将其转化成字符串形式
datetime.strftime('字符串时间'，'时间格式化字符')#将字符串转化成时间datetime时间
datetime.fromtimestamp()#时间戳转datetime
.timestamp()#datetime转时间戳
datetime.now()#本地时间
datetime.utcnow()#utc时间
time.sleep()#等待秒数

