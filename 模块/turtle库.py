import turtle
turtle.setup(0.5,0.5)
#.setup(长（如果是小数，表示窗口长度与屏幕长度比列），宽（如果是小数，表示窗口宽度与屏幕宽度比列），左边距（如果是none，位于屏幕水平中央），上边距（如果是none，位于垂直中间）) 设置画布大小
#.penup()抬起画笔，简写up(),pu()
#.pendown()放下画笔，简写pd(),down()
#.pensize()设置画笔大小，简写.width(),无参数时返回当前画笔宽度
#.pencolor()设置画笔颜色
