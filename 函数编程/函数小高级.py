#!/usr/bin/env python 
#-*- coding:utf-8 -*-
def func():
    print(123)
zhang = func
func()
zhang()
# 函数名其实也算是一种数据类型
l1 = [func,zhang]
l1[0]()
l1[1]()
# 列表可以用来存放函数名
# 当列表中存放的是这种形式的时候，列表里面的值则是函数的返回值
l1 = [func(),zhang()]
print(l1)
# 元组同样可以用来存放函数名，但是由于元组自带去重，所以说一般不用它来存放
dic = {'k1':func,'k2':zhang}
dic['k1']()
# 字典里面同样可以用于存放函数名，但是如果跟的是函数名（）则一样是等于函数的返回值

