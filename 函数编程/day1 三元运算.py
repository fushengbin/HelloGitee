基本格式：v = if 条件 else 后面
简单实例:让用户输入值,如果值是整数,则转化成整数,否者赋值为None
a = input('>>>')
value = int(a) if a.isdecimal() else None
print(value)
其实说白了,三元运算就是在有if else 赋值的时候的简单写法