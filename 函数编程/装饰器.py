#!/usr/bin/env python 
#-*- coding:utf-8 -*-
# 装饰器：在不改变原函数代码基础上，在函数执行之前和之后自动执行某个功能
# 这里还讲了一个time模块
# time.time()获取当前时间
# time.sleep()暂停