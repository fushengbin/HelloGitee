#!/usr/bin/env python
#-*- coding:utf-8 -*-
# 位置传参（调用函数并传入参数）：
def one_func(a1,a2):
    print(a1,a2)
one_func(1,2)
# 关键字传参(不用管函数传入参数的位置,当关键字传参和位置参数在一起用的时候,传参一定要在后面):
def one_func(a1,a2):
    print(a1,a2)
one_func(a2=1,a1= 2)
# 函数的默认参数(这里的a2就是默认参数,你可以对其进行位置传参或者关键字传参进行改变,如果不,则就是默认值):
def one_func(a1,a2=2):
    print(a1,a2)
one_func(1)
# 万能参数（定义函数的时候在参数前面加一个*，可以接受多种类型的元素，不能接受关键字传参）：
def one_func(*a):#这种一般用*args表示
    print(a),
one_func(1,['dsa'])
#可以在定义的时候和关键字一起,但是关键字在前面的话,就只能通过位置传确认
# 万能参数(2)(这个是将传入的参数作为字典的值,可以接受多个参数):
def one_func(**a):
    print(a),
one_func(zhang='hubin')
# 如果直接one_func(**{k1 = 'eq',k2 = 'dsadas'})也是可以的,即可以直接跟字典