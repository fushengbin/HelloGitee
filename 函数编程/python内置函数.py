#!/usr/bin/env python 
#-*- coding:utf-8 -*-
# 函数传递的是内存地址
# 函数不被调用内部代码不执行
# folat 转化成小数(就是在后面加.0):
v = 22
print(float(v))
# max/min找最大/小值
v1 = [213,231,4,1421,41,41,411321]
print(max(v1))
print(min(v1))
# sum求和：
print(sum(v1))
# divmod两数相除，求商和余数：
v2 = 23123
print(divmod(v2,v))
# bin将十进制转化成二进制
# oct将十进制转化成八进制
# int将其它进制转化成十进制，要加base跟数字即几进制
# hex将十进制转化成十六进制
# pow求次方
print(pow(2,3))
# round求指定的几位小数
print(round(1.2334,3))
# 函数内部执行相互之间不会影响，当执行完毕且内部元素不被他人使用就销毁
# 闭包的概念：为函数创建一个区域并为其维护自己的数据，以后执行方便调用
print(chr(99))
# 将十进制数字转化成Unicode编码中对应字符
print(ord('中'))
# 将该字符在Unicode中对应的二进制转化成十进制