#!/usr/bin/env python 
#-*- coding:utf-8 -*-
# lanbda表达式：为了解决简单函数
# 如:
def func(a1,a2):
    return a1+a2
func1 = lambda a1,a2:a1+a2
print(func(1,2))
print(func1(2,3))
# 个人觉得其实lambda表达式就是定义函数然后加return的简写(基本上)lambda表达式又称为匿名函数