# return 在函数中return可以定义返回值(默认为None，函数在执行中遇到return则停止该函数)
# 例如：
def print_number(a):
    print(a)
    return a
c = print_number('a')
print(c)
# 当为这种时返回值为元组
def print_number(a):
    print(a)
    return 1,12,3,1,23,4,1