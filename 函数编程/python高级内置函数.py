#!/usr/bin/env python
#-*- coding:utf-8 -*-
l1 = [123,321,321,41,4]
result = map(lambda x:x+100,l1)
print(list(result))
# map:循环第二个参数中的每个元素，然后在循环的过程中执行该函数，并将每个元素执行的结果添加到一个列表中，并返回（并不影响第二个参数里面的元素）
# 但是返回值不能直接查看（py3中不能，py2中可以），必须先将其转化成list，其实就相当于for 循环的时候执行函数，然后的简便方法
# filter和map基本一致，只不过是将返回值为Ture的添加到列表中，
# reduce 和上面基本一致，只不过这个函数有要求，即是相加或者相乘那种，就计算该参数中所有元素的乘或和
# 因为python3中不内置,所以需要下列操作(1.导入模块,2,运行reduce):
        # import functools
        # functools.reduce()
    